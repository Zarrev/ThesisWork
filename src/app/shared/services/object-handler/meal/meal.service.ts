import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {NetworkService} from '../../network/network.service';
import {MealFirebaseService} from '../../database/firebase/meal/meal-firebase.service';
import {MealIndexedDbService} from '../../database/local/meal/meal-indexed-db.service';
import {BaseService} from '../base/base.service';
import {Meal} from '../../../models/meal.interface';
import {MatSnackBar} from '@angular/material';
import {openSnackBar} from '../../../utils/snackbar';
import {first} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MealService extends BaseService {

  private _currentMeal = new Subject<Meal>();
  private _allMeal = new Subject<Meal[]>();
  private appUserId: string;

  constructor(private firebaseService: MealFirebaseService, private indexedDbService: MealIndexedDbService,
              private networkService: NetworkService, private _snackBar: MatSnackBar) {
    super();
    this.subsc.push(this.firebaseService.allMeal.subscribe(value => {
      this._allMeal.next(value);
      if (value) {
        this.numFBLength = value.length;
      } else {
        this.numFBLength = 0;
      }
    }));
    this.subsc.push(this.firebaseService.currentMeal.subscribe(value => this._currentMeal.next(value)));
    this.subsc.push(this.indexedDbService.allMeal.subscribe(value => {
      this._allMeal.next(value);
      if (value) {
        this.numIDBLength = value.length;
      } else {
        this.numIDBLength = 0;
      }
    }));
    this.subsc.push(this.indexedDbService.currentMeal.subscribe(value => this._currentMeal.next(value)));
  }

  public setAppUserId(uid: string) {
    this.appUserId = uid;
    if (uid) {
      this.firebaseService.getAllMeal(this.appUserId).pipe(first()).subscribe(
        meals => this.indexedDbService.syncLocalMeals(meals),
        error => openSnackBar(this._snackBar, 'Error: ' + error));
    }
  }

  public delete(meal: Meal) {
    if (this.networkService.isOnline) {
      this.firebaseService.deleteMeal(this.appUserId, meal);
    } else {
      this.indexedDbService.deleteMeal(meal);
    }
    openSnackBar(this._snackBar, meal.name + ' has deleted');
  }

  public get(mealKey: string): Observable<Meal> {
    if (this.networkService.isOnline && !this.isDownloaded()) {
      this.firebaseService.getMeal(this.appUserId, mealKey);
    } else {
      this.indexedDbService.getMeal(mealKey);
    }

    return this.currentMeal;
  }

  public getAll(): Observable<Meal[]> {
    if (this.networkService.isOnline && !this.isDownloaded()) {
      this.firebaseService.getAllMeal(this.appUserId);
    } else {
      this.indexedDbService.getAllMeal();
    }

    return this.allMeal;
  }

  public post(meal: Meal): string | number {
    if (this.networkService.isOnline) {
      const key: string = this.firebaseService.postMeal(this.appUserId, meal);
      const data: Meal = {
        $key: key,
        date: meal.date,
        name: meal.name,
        rate: meal.rate,
        picture: meal.picture
      };
      this.indexedDbService.postMeal(data);
      openSnackBar(this._snackBar, data.name + ' has added!');
      return key;
    } else {
      openSnackBar(this._snackBar, meal.name + ' has added!');
      return this.indexedDbService.postMealSync(meal);
    }
  }

  public put(meal: Meal) {
    if (this.networkService.isOnline) {
      const key: string = this.firebaseService.putMeal(this.appUserId, meal);
      const data: Meal = {
        $key: key,
        date: meal.date,
        name: meal.name,
        rate: meal.rate,
        picture: meal.picture
      };
      this.indexedDbService.putMeal(data);
    } else {
      this.indexedDbService.putMeal(meal);
      this.indexedDbService.putMealSync(meal);
    }
    openSnackBar(this._snackBar, meal.name + ' has updated!');
  }

  public clean() {
    this.firebaseService.clean();
    this.indexedDbService.clean();
    this._currentMeal.next(null);
    this._allMeal.next(null);
  }

  public turnOnSync(): void {
    openSnackBar(this._snackBar, 'Syncing in the background, keep online!');
    this.subsc.push(this.networkService.connectionChanged.subscribe(isOnline => {
      if (isOnline) {
        this.indexedDbService.syncMeal((meal) => {
          this.firebaseService.postMeal(this.appUserId, meal);
        });
      }
    }));
  }

  // public keepUpToDateMeals(): void {
  //   if (this.networkService.isOnline) {
  //     this.subsc.push(this.getAll().subscribe(
  //       meals => this.indexedDbService.syncLocalMeals(meals),
  //       error => openSnackBar(this._snackBar, 'Error: ' + error),
  //       () => openSnackBar(this._snackBar, 'Complete: Keeping up to date your meals.')));
  //   }
  // }

  get currentMeal(): Observable<Meal> {
    return this._currentMeal.asObservable();
  }

  get allMeal(): Observable<Meal[]> {
    return this._allMeal.asObservable();
  }
}
