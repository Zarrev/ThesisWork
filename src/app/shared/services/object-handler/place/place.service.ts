import {Injectable} from '@angular/core';
import {BaseService} from '../base/base.service';
import {Observable, Subject} from 'rxjs';
import {NetworkService} from '../../network/network.service';
import {PlaceFirebaseService} from '../../database/firebase/place/place-firebase.service';
import {PlaceIndexedDbService} from '../../database/local/place/place-indexed-db.service';
import {Place} from '../../../models/place.interface';
import {openSnackBar} from '../../../utils/snackbar';
import {MatSnackBar} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class PlaceService extends BaseService {

  private _currentPlace = new Subject<Place>();
  private _allPlace = new Subject<Place[]>();
  private appUserId: string;

  constructor(private firebaseService: PlaceFirebaseService, private indexedDbService: PlaceIndexedDbService,
              private networkService: NetworkService, private _snackBar: MatSnackBar) {
    super();
    this.subsc.push(this.firebaseService.allPlace.subscribe(value => this._allPlace.next(value)));
    this.subsc.push(this.firebaseService.currentPlace.subscribe(value => this._currentPlace.next(value)));
    this.subsc.push(this.indexedDbService.allPlace.subscribe(value => this._allPlace.next(value)));
    this.subsc.push(this.indexedDbService.currentPlace.subscribe(value => this._currentPlace.next(value)));
  }

  public setAppUserId(uid: string) {
    this.appUserId = uid;
    if (uid) {
      this.firebaseService.getAllPlace(this.appUserId).subscribe(places => this.indexedDbService.syncLocalPlaces(places),
      error => openSnackBar(this._snackBar, 'Error: ' + error));
    }
  }

  public delete(place: Place) {
    if (this.networkService.isOnline) {
      this.firebaseService.deletePlace(this.appUserId, place);
    } else {
      this.indexedDbService.deletePlace(place);
    }
    openSnackBar(this._snackBar, place.name + ' has deleted!');
  }

  public get(placeKey: string): Observable<Place> {
    if (this.networkService.isOnline && !this.isDownloaded()) {
      this.firebaseService.getPlace(this.appUserId, placeKey);
    } else {
      this.indexedDbService.getPlace(placeKey);
    }

    return this.currentPlace;
  }

  public getAll(): Observable<Place[]> {
    if (this.networkService.isOnline && !this.isDownloaded()) {
      this.firebaseService.getAllPlace(this.appUserId);
    } else {
      this.indexedDbService.getAllPlace();
    }

    return this.allPlace;
  }

  public post(place: Place): string | number {
    if (this.networkService.isOnline) {
      const key: string = this.firebaseService.postPlace(this.appUserId, place);
      const data: Place = {
        $key: key,
        name: place.name,
        avgRate: place.avgRate,
        mealKeys: place.mealKeys,
        marker: place.marker
      };
      this.indexedDbService.postPlace(data);
      openSnackBar(this._snackBar, place.name + ' has added!');
      return key;
    } else {
      openSnackBar(this._snackBar, place.name + ' has added!');
      return this.indexedDbService.postPlaceSync(place);
    }
  }

  public put(place: Place, modifiedField?: string) {
    if (this.networkService.isOnline) {
      let key = '';
      if (modifiedField) {
        key = this.firebaseService.putPlace(this.appUserId, place, modifiedField);
      } else {
        key = this.firebaseService.putPlace(this.appUserId, place);
      }
      const data: Place = {
        $key: key,
        name: place.name,
        avgRate: place.avgRate,
        mealKeys: place.mealKeys,
        marker: place.marker
      };
      this.indexedDbService.putPlace(data);
    } else {
      this.indexedDbService.putPlace(place);
      this.indexedDbService.putPlaceSync(place);
    }
    openSnackBar(this._snackBar, place.name + ' has updated!');
  }

  public clean() {
    this.firebaseService.clean();
    this.indexedDbService.clean();
    this._currentPlace.next(null);
    this._allPlace.next([]);
  }

  public turnOnSync(): void {
    openSnackBar(this._snackBar, 'Syncing in the background, keep online!');
    this.subsc.push(this.networkService.connectionChanged.subscribe(isOnline => {
      if (isOnline) {
        this.indexedDbService.syncPlace((place) => {
          this.firebaseService.postPlace(this.appUserId, place);
        });
      }
    }));
  }

  // public keepUpToDatePlaces(): void {
  //   if (this.networkService.isOnline) {
  //     this.subsc.push(this.getAll().subscribe(places => this.indexedDbService.syncLocalPlaces(places),
  //       error => openSnackBar(this._snackBar, 'Error: ' + error),
  //       () => openSnackBar(this._snackBar, 'Complete: Keeping up to date your places.')));
  //   }
  // }

  get currentPlace(): Observable<Place> {
    return this._currentPlace.asObservable();
  }

  get allPlace(): Observable<Place[]> {
    return this._allPlace.asObservable();
  }

  setCurrentPlace(place: Place) {
    this._currentPlace.next(place);
  }
}
