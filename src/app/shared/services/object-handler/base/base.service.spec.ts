import {async, TestBed} from '@angular/core/testing';

import { BaseService } from './base.service';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';

describe('BaseService', () => {
  beforeEach(async(() => TestBed.configureTestingModule({
    imports: [
      AngularFireModule.initializeApp(environment.firebaseConfig),
      AngularFireDatabaseModule
    ],
    providers: [
      BaseService
    ]
  })));

  it('should be created', () => {
    const service: BaseService = TestBed.get(BaseService);
    expect(service).toBeTruthy();
  });
});
