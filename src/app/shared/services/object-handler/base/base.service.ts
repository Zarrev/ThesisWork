import {Subscription} from 'rxjs';
import {Injectable, OnDestroy} from '@angular/core';

@Injectable()
export abstract class BaseService implements OnDestroy {

  protected subsc: Subscription[] = [];
  protected numIDBLength: number;
  protected numFBLength: number;

  protected constructor() { }

  public abstract post(item: any);

  public abstract getAll();

  public abstract get(item: any);

  public abstract put(item: any);

  public abstract delete(item: any);

  public abstract clean();

  public abstract turnOnSync(item: any);

  ngOnDestroy(): void {
    this.subsc.forEach(x => x.unsubscribe());
  }

  isDownloaded(): boolean {
    return this.numIDBLength && this.numFBLength && this.numIDBLength === this.numFBLength;
  }
}
