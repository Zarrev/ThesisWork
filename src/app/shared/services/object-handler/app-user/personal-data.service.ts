import {Injectable} from '@angular/core';
import {AppUser, Profile, Settings} from '../../../models/app-user.interface';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {auth, User as firebaseUser} from 'firebase/app';
import {BaseService} from '../base/base.service';
import {NetworkService} from '../../network/network.service';
import {PersonalDataFirebaseService} from '../../database/firebase/personal-data/personal-data.service';
import {PersonalDataIndexedDbService} from '../../database/local/personal-data/personal-data.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {MealService} from '../meal/meal.service';
import {PlaceService} from '../place/place.service';
import {openSnackBar} from '../../../utils/snackbar';
import {first} from 'rxjs/operators';
import {UsersService} from '../../database/firebase/community/users.service';

@Injectable({
  providedIn: 'root'
})
export class PersonalDataService extends BaseService {

  private _defaultProfile: Profile = {profilePicture: null, nickname: 'No provided nickname!'};
  private _defaultSettings: Settings = {gps: false, notifyNumber: 3};
  private _fireUser: firebaseUser = null;
  private defaultAppUser: AppUser = {
    fireUser: this._fireUser,
    profile: this._defaultProfile,
    settings: this._defaultSettings,
    placeKeys: []
  };
  private _user = new BehaviorSubject<AppUser>(this.defaultAppUser);
  private _isLoggedIn = new BehaviorSubject<boolean>(false);

  constructor(private firebaseService: PersonalDataFirebaseService, private indexedDbService: PersonalDataIndexedDbService,
              private networkService: NetworkService, private afAuth: AngularFireAuth, private _snackBar: MatSnackBar,
              private router: Router, private mealService: MealService, private placeService: PlaceService,
              private usersService: UsersService) {
    super();
    this.autoLogIn();
    this.subsc.push(this.firebaseService.currentAppUser.subscribe(value => {
      let validValue = this.initValidAppUser(value);
      this.defaultAppUser = validValue ? validValue : this.defaultAppUser;
      this._user.next(validValue);
      if (validValue) {
        usersService.saveAppUser(validValue);
      }
    }));
    this.subsc.push(this.indexedDbService.currentAppUser.subscribe(value => {
      let validValue = this.initValidAppUser(value);
      this.defaultAppUser = validValue ? validValue : this.defaultAppUser;
      this._user.next(validValue);
    }));
  }

  private initValidAppUser(value: any): AppUser {
    if (!value) {
      return null;
    }
    const placeKeys: string[] = [];
    for (const prop in value.placeKeys) {
      if (Object.prototype.hasOwnProperty.call(value.placeKeys, prop)) {
        placeKeys.push(value.placeKeys[prop]);
      }
    }
    const user: AppUser = {
      fireUser: this._fireUser,
      placeKeys,
      settings: value.settings,
      profile: value.profile
    };

    if (!user.placeKeys) {
      user.placeKeys = [];
    }
    if (!user.profile) {
      user.profile = this.defaultAppUser.profile;
    }
    return user;
  }

  private persistInLocal(): Promise<void> {
    return this.afAuth.auth.setPersistence(auth.Auth.Persistence.LOCAL);
  }

  private isLoggedInUser(user: firebaseUser): boolean {
    return user !== null && user !== undefined;
  }

  private autoLogIn() {
    this.persistInLocal().then(() => {
      this.subsc.push(this.afAuth.authState.subscribe(user => {
        const isLoggedIn = this.isLoggedInUser(user);
        this._isLoggedIn.next(isLoggedIn);
        if (!isLoggedIn) {
          return;
        }
        openSnackBar(this._snackBar, 'Welcome, ' + user.displayName);
        this._fireUser = user;
        this.mealService.setAppUserId(user.uid);
        this.placeService.setAppUserId(user.uid);
        this.subsc.push(this.get().subscribe(value => {
          if (!value) {
            this.defaultAppUser.fireUser = user;
            this.post(this.defaultAppUser);
          }
        }));
        this.keepUpToDateAppUser(user.uid);
      }));
    }, err => {
      openSnackBar(this._snackBar, err);
    });
  }

  public signIn(): void {
    const provider = new auth.FacebookAuthProvider();
    this.persistInLocal().then(() => {
      this.afAuth.auth.signInWithPopup(provider).then(res => {
        openSnackBar(this._snackBar, res.user.displayName + ' has logged in!');
      }, err => {
        openSnackBar(this._snackBar, err);
      });
    });
  }

  public signOut(redirection: string = 'main'): void {
    this.afAuth.auth.signOut().then(() => {
      this.mealService.clean();
      this.placeService.clean();
      this.clean();
      openSnackBar(this._snackBar, 'Logged out!');
    }, err => {
      openSnackBar(this._snackBar, err);
    }).finally(() => {
      this.router.navigate([redirection]);
    });
  }

  public delete(appUser: AppUser) {
    // console.log('this function is not needed/implemented!');
  }

  public putPlaceKey(placeKey: string) {
    this.defaultAppUser.placeKeys.push(placeKey);
    this.post(this.defaultAppUser);
  }

  public delPlaceKey(placeKey: string) {
    this.defaultAppUser.placeKeys = this.defaultAppUser.placeKeys.filter(x => x !== placeKey);
    this.post(this.defaultAppUser);
  }

  public get(): Observable<AppUser> {
    if (this.networkService.isOnline) {
      this.firebaseService.getAppUser(this._fireUser.uid);
    } else {
      this.indexedDbService.getAppUser();
    }

    return this.user;
  }

  public getAll(): Observable<AppUser[]> {
    // console.log('this function is not needed/implemented!');
    return of([this.defaultAppUser]);
  }

  public post(appUser: AppUser) {
    if (this.networkService.isOnline) {
      const key: string = this.firebaseService.postAppUser(this._fireUser.uid, appUser);
      this.indexedDbService.postAppUser(appUser);
      return key;
    } else {
      return this.indexedDbService.postAppUserSync(appUser);
    }
  }

  public put(appUser: AppUser) {
    if (this.networkService.isOnline) {
      const key: string = this.firebaseService.postAppUser(this._fireUser.uid, appUser);
      this.indexedDbService.putAppUser(appUser);
    } else {
      this.indexedDbService.putAppUserSync(appUser);
    }
  }

  public clean() {
    this.defaultAppUser = {
      fireUser: null,
      profile: null,
      settings: null
    };
    this._user.next(null);
    this._isLoggedIn.next(false);
  }

  public turnOnSync(): void {
    this.subsc.push(this.networkService.connectionChanged.subscribe(isOnline => {
      if (isOnline) {
        this.indexedDbService.syncAppUser((appUser) => {
          this.firebaseService.postAppUser(this._fireUser.uid, appUser);
        });
      }
    }));
  }

  public keepUpToDateAppUser(uid: string): void {
    this.firebaseService.getAppUser(uid).pipe(first()).subscribe(
      appUser => this.indexedDbService.syncLocalAppUser(this.initValidAppUser(appUser)),
      error => openSnackBar(this._snackBar, 'Error: ' + error));
  }

  get user(): Observable<AppUser> {
    return this._user.asObservable();
  }

  get isLoggedIn(): Observable<boolean> {
    return this._isLoggedIn.asObservable();
  }
}
