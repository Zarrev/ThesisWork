import {async, TestBed} from '@angular/core/testing';

import { PersonalDataService } from './personal-data.service';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {NearNotifierService} from '../../../../features/hungry/services/push-noti/near-notifier.service';
import {MatSnackBarModule} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';

describe('PersonalDataService', () => {
  beforeEach(async(() => TestBed.configureTestingModule({
    imports: [
      AngularFireModule.initializeApp(environment.firebaseConfig),
      AngularFireDatabaseModule,
      AngularFireAuthModule,
      MatSnackBarModule,
      RouterTestingModule
    ],
    providers: [
      NearNotifierService
    ]
  })));

  it('should be created', () => {
    const service: PersonalDataService = TestBed.get(PersonalDataService);
    expect(service).toBeTruthy();
  });
});
