import {async, TestBed} from '@angular/core/testing';

import { LoadingService } from './loading.service';
import {OverlayModule} from '@angular/cdk/overlay';

describe('LoadingService', () => {
  beforeEach(async(() => TestBed.configureTestingModule({
    imports: [
      OverlayModule
    ],
    providers: [
      LoadingService
    ]
  })));

  it('should be created', () => {
    const service: LoadingService = TestBed.get(LoadingService);
    expect(service).toBeTruthy();
  });
});
