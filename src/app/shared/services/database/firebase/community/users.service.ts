import {Injectable} from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {Users} from '../../../../models/community-models/users.interface';
import {Observable} from 'rxjs';
import {AppUser} from '../../../../models/app-user.interface';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private usersRef: AngularFireList<Users>;
  private readonly path = 'users';
  messaging = firebase.messaging();

  constructor(private db: AngularFireDatabase) {
    this.usersRef = db.list<Users>(this.path);
  }

  getReceiverUser(receiverEmail: string): Observable<Users[]> {
    return this.db.list<Users>(this.path, ref => {
      return ref.orderByChild('email').equalTo(receiverEmail);
    }).valueChanges();
  }

  saveAppUser(appUser: AppUser) {
    let userFromAppUser: Users;
    this.messaging.requestPermission().then(() => {
      return this.messaging.getToken();
    })
      .then(token => {
        console.log(token);
        userFromAppUser = {email: appUser.fireUser.email, key: appUser.fireUser.uid, fcmToken: token};
        this.usersRef.set(appUser.fireUser.uid, userFromAppUser);
      })
      .catch((err) => {
        console.log('Unable to get permission to notify', err);
      });
    userFromAppUser = {email: appUser.fireUser.email, key: appUser.fireUser.uid};
    this.usersRef.set(appUser.fireUser.uid, userFromAppUser);
  }
}
