import { TestBed } from '@angular/core/testing';

import { MessagesService } from './messages.service';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';

describe('MessagesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AngularFireModule.initializeApp(environment.firebaseConfig),
      AngularFireDatabaseModule
    ]
  }));

  it('should be created', () => {
    const service: MessagesService = TestBed.get(MessagesService);
    expect(service).toBeTruthy();
  });
});
