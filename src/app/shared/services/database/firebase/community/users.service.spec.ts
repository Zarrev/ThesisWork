import {TestBed} from '@angular/core/testing';

import {UsersService} from './users.service';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';

describe('UsersService', () => {
  beforeEach(() => {
    // firebase.initializeApp(environment.firebaseConfig);
    TestBed.configureTestingModule({
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule
      ]
    });
  });

  it('should be created', () => {
    const service: UsersService = TestBed.get(UsersService);
    expect(service).toBeTruthy();
  });
});
