import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {Observable} from 'rxjs';
import {Message} from '../../../../models/community-models/message.interface';
import ThenableReference = firebase.database.ThenableReference;

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  private readonly path = '/messages';

  constructor(private db: AngularFireDatabase) {
  }

  getMyMessages(myKey: string, xMultTen: number = 1): Observable<Message[]> {
    const limit = 10 * xMultTen;
    return this.db.list<Message>(myKey + this.path, ref => {
      return ref.orderByChild('timestamp').limitToLast(limit);
    }).valueChanges();
  }

  sendMyMessage(receiverKey: string, message: Message): ThenableReference {
    return this.db.list<Message>(receiverKey + this.path).push(message);
  }
}
