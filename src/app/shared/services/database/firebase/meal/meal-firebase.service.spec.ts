import { TestBed } from '@angular/core/testing';
import {MealFirebaseService} from './meal-firebase.service';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';


describe('MealFirebaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AngularFireModule.initializeApp(environment.firebaseConfig),
      AngularFireDatabaseModule
    ]
  }));

  it('should be created', () => {
    const service: MealFirebaseService = TestBed.get(MealFirebaseService);
    expect(service).toBeTruthy();
  });
});
