import {BaseFirebaseService} from '../base/base-firebase.service';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {AngularFireDatabase} from '@angular/fire/database';
import {Injectable} from '@angular/core';
import {Meal} from '../../../../models/meal.interface';

@Injectable({
  providedIn: 'root'
})
export class MealFirebaseService extends BaseFirebaseService {

  private _currentMeal = new Subject<Meal>();
  private _allMeal = new Subject<Meal[]>();

  constructor(db: AngularFireDatabase) {
    super(db);
  }

  private initValidMeal(value: any): Meal {
    return {
      $key: value.$key,
      date: value.date,
      rate: value.rate,
      name: value.name,
      picture: value.picture
    };
  }

  private initValidMeals(values: any[]): Meal[] {
    const meals: Meal[] = [];
    values.forEach(x => meals.push(this.initValidMeal(x)));
    return meals;
  }

  public clean(): void {
    this._currentMeal.next(null);
    this._allMeal.next([]);
  }

  // CRUD operation: GET (single)
  public getMeal(userID: string, key: string): Observable<Meal> {
    super.getDataFromFirebase(userID + '/meals', key).subscribe(value => {
      this._currentMeal.next(this.initValidMeal(value));
    });

    return this._currentMeal.asObservable();
  }

  // CRUD operation: GET (all)
  public getAllMeal(userID: string): Observable<Meal[]> {
    super.getDataArrayFromFirebase(userID + '/meals').subscribe(value => {
      this._allMeal.next(this.initValidMeals(value));
    });

    return this._allMeal.asObservable();
  }

  // CRUD operation: POST
  public postMeal(userID: string, data: Meal): string {
    return super.postDataFromFirebase(userID + '/meals', {
      name: data.name,
      rate: data.rate,
      date: data.date,
      picture: data.picture
    }).key;
  }

  // CRUD operation: PUT
  public putMeal(userID: string, data: Meal): string {
    return super.putDataFromFirebase(userID + '/meals', data.$key, {
      name: data.name,
      rate: data.rate,
      date: data.date,
      picture: data.picture
    });
  }

  // CRUD operation: DELETE
  public deleteMeal(userID: string, data: Meal): string {
    return super.deleteDataFromFirebase(userID + '/meals', data.$key);
  }


  get currentMeal(): Observable<Meal> {
    return this._currentMeal.asObservable();
  }

  get allMeal(): Observable<Meal[]> {
    return this._allMeal.asObservable();
  }
}
