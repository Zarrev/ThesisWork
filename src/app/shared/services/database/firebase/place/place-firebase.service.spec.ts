import { TestBed } from '@angular/core/testing';

import { PlaceFirebaseService } from './place-firebase.service';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';

describe('PlaceFirebaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AngularFireModule.initializeApp(environment.firebaseConfig),
      AngularFireDatabaseModule
    ]
  }));

  it('should be created', () => {
    const service: PlaceFirebaseService = TestBed.get(PlaceFirebaseService);
    expect(service).toBeTruthy();
  });
});
