import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {BaseFirebaseService} from '../base/base-firebase.service';
import {AngularFireDatabase} from '@angular/fire/database';
import {Place} from '../../../../models/place.interface';

@Injectable({
  providedIn: 'root'
})
export class PlaceFirebaseService extends BaseFirebaseService {

  private _currentPlace = new Subject<Place>();
  private _allPlace = new Subject<Place[]>();

  constructor(db: AngularFireDatabase) {
    super(db);
  }

  private initValidPlace(value: any): Place {
    const mealKeys: string[] = [];
    for (const prop in value.mealKeys) {
      if (Object.prototype.hasOwnProperty.call(value.mealKeys, prop)) {
        mealKeys.push(value.mealKeys[prop]);
      }
    }
    return {
      $key: value.$key,
      marker: value.marker,
      name: value.name,
      avgRate: value.avgRate,
      mealKeys
    };
  }

  private initValidPlaces(values: any[]): Place[] {
    const places: Place[] = [];
    values.forEach(x => places.push(this.initValidPlace(x)));
    return places;
  }

  public clean(): void {
    this._currentPlace.next(null);
    this._allPlace.next([]);
  }

  // CRUD operation: GET (single)
  public getPlace(userID: string, key: string): Observable<Place> {
    super.getDataFromFirebase(userID + '/places', key).subscribe(value => {
      this._currentPlace.next(this.initValidPlace(value));
    });

    return this._currentPlace.asObservable();
  }

  // CRUD operation: GET (all)
  public getAllPlace(userID: string): Observable<Place[]> {
    super.getDataArrayFromFirebase(userID + '/places').subscribe(value => {
      this._allPlace.next(this.initValidPlaces(value));
    });

    return this._allPlace.asObservable();
  }

  // CRUD operation: POST
  public postPlace(userID: string, data: Place): string {
    return super.postDataFromFirebase(userID + '/places', {
      marker: {longitude: data.marker.longitude, latitude: data.marker.latitude},
      mealKeys: data.mealKeys,
      name: data.name,
      avgRate: data.avgRate,
    }).key;
  }

  // CRUD operation: PUT
  public putPlace(userID: string, data: Place, modifiedField?: string): string {
    const sendData = {
      marker: {longitude: data.marker.longitude, latitude: data.marker.latitude},
      mealKeys: data.mealKeys,
      name: data.name,
      avgRate: data.avgRate,
    };
    if (modifiedField) {
      return super.putDataFromFirebase(userID + '/places', data.$key, sendData, modifiedField);
    }

    return super.putDataFromFirebase(userID + '/places', data.$key, sendData);
  }

  // CRUD operation: DELETE
  public deletePlace(userID: string, data: Place): string {
    return super.deleteDataFromFirebase(userID + '/places', data.$key);
  }


  get currentPlace(): Observable<Place> {
    return this._currentPlace.asObservable();
  }

  get allPlace(): Observable<Place[]> {
    return this._allPlace.asObservable();
  }
}
