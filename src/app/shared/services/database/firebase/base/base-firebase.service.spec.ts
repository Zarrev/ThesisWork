import {async, TestBed} from '@angular/core/testing';

import { BaseFirebaseService } from './base-firebase.service';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';

describe('BaseFirebaseService', () => {
  beforeEach(async(() => TestBed.configureTestingModule({
    imports: [
      AngularFireModule.initializeApp(environment.firebaseConfig),
      AngularFireDatabaseModule
    ],
    providers: [
      BaseFirebaseService
    ]
  })));

  it('should be created', () => {
    const service: BaseFirebaseService = TestBed.get(BaseFirebaseService);
    expect(service).toBeTruthy();
  });
});
