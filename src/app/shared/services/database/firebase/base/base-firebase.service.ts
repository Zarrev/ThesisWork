import {AngularFireAction, AngularFireDatabase, AngularFireList, AngularFireObject, DatabaseSnapshot} from '@angular/fire/database';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Injectable} from '@angular/core';

@Injectable()
export abstract class BaseFirebaseService {

  protected constructor(private db: AngularFireDatabase) {
  }

  protected getDataArrayFromFirebase(pathOrRef: string): Observable<any[]> {
    const loadedDataRef: AngularFireList<any> = this.db.list(pathOrRef);
    return loadedDataRef.snapshotChanges().pipe(map(
      data => {
        if (data === undefined || data === null || Object.keys(data).length === 0) {
          return [];
        }
        const loadedDataArray: any = [];
        let loadedData: any;
        (data as AngularFireAction<DatabaseSnapshot<any>>[]).forEach(item => {
          loadedData = (item.payload.toJSON());
          loadedData.$key = item.key;
          loadedDataArray.push(loadedData);
        });
        return (loadedDataArray);
      }),
      catchError(err => {
        // console.log(err);
        return of([]);
      }));
  }

  protected getDataFromFirebase(pathOrRef: string, key: string): Observable<any> {
    const loadedDataRef: AngularFireObject<any> = this.db.object(pathOrRef + '/' + key);
    return loadedDataRef.snapshotChanges().pipe(map(
      data => {
        if (data === undefined || data === null || Object.keys(data).length === 0) {
          return undefined;
        }
        let loadedData: any;
        loadedData = ((data as AngularFireAction<DatabaseSnapshot<any>>).payload.toJSON());
        loadedData.$key = (data as AngularFireAction<DatabaseSnapshot<any>>).key;
        return loadedData;
      }),
      catchError(err => {
        // console.log(err);
        return of(null);
      }));
  }

  protected getDataToFirebase(pathOrRef: string): Observable<any> {
    const loadedDataRef: AngularFireObject<any> = this.db.object(pathOrRef);
    return loadedDataRef.valueChanges();
  }

  protected postDataFromFirebase(pathOrRef: string, data: any): firebase.database.ThenableReference {
    const loadedDataRef: AngularFireList<any> = this.db.list(pathOrRef);
    return loadedDataRef.push(data);
  }

  protected postDataToFirebase(pathOrRef: string, data: any): Promise<any> {
    const loadedDataRef: AngularFireObject<any> = this.db.object(pathOrRef);
    return loadedDataRef.set(data);
  }

  protected putDataFromFirebase(pathOrRef: string, key: string, data: any, optionalAdditionalPath?: string): string {
    let finalPathRef = pathOrRef + '/' + key;
    if (optionalAdditionalPath) {
      finalPathRef += '/' + optionalAdditionalPath;
    }
    const loadedDataRef: AngularFireObject<any> = this.db.object(finalPathRef);
    loadedDataRef.update(data).catch(reason => {
      // console.log(reason);
    });
    return key;
  }

  protected deleteDataFromFirebase(pathOrRef: string, key: string): string {
    const loadedDataRef: AngularFireObject<any> = this.db.object(pathOrRef + '/' + key);
    loadedDataRef.remove().catch(reason => {
      // console.log(reason);
    });
    return key;
  }

  protected abstract clean(): void;
}
