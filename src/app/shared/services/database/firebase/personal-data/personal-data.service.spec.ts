import { TestBed } from '@angular/core/testing';

import { PersonalDataFirebaseService } from './personal-data.service';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';

describe('PersonalDataFirebaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AngularFireModule.initializeApp(environment.firebaseConfig),
      AngularFireDatabaseModule
    ]
  }));

  it('should be created', () => {
    const service: PersonalDataFirebaseService = TestBed.get(PersonalDataFirebaseService);
    expect(service).toBeTruthy();
  });
});
