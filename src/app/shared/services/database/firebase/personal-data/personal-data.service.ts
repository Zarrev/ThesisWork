import { Injectable } from '@angular/core';
import {BaseFirebaseService} from '../base/base-firebase.service';
import {Observable, Subject} from 'rxjs';
import {AngularFireDatabase} from '@angular/fire/database';
import {AppUser} from '../../../../models/app-user.interface';

@Injectable({
  providedIn: 'root'
})
export class PersonalDataFirebaseService extends BaseFirebaseService  {

  private _currentAppUser = new Subject<AppUser>();

  constructor(db: AngularFireDatabase) {
    super(db);
  }

  public clean(): void {
    this._currentAppUser.next(null);
  }

  public getAppUser(userID: string): Observable<AppUser> {
    super.getDataToFirebase(userID + '/personalData').subscribe(value => {
      this._currentAppUser.next(value);
    });

    return this._currentAppUser.asObservable();
  }

  public postAppUser(userID: string, data: AppUser): string {
    super.postDataToFirebase(userID + '/personalData', {
      settings: data.settings,
      profile: data.profile,
      placeKeys: data.placeKeys
    });
    return data.fireUser.uid;
  }

  get currentAppUser(): Observable<AppUser> {
    return this._currentAppUser.asObservable();
  }
}
