import { Injectable } from '@angular/core';
import {BaseIndexedDbService} from '../base/base-indexed-db.service';
import {Observable, Subject} from 'rxjs';
import {AppUser} from '../../../../models/app-user.interface';

@Injectable({
  providedIn: 'root'
})
export class PersonalDataIndexedDbService extends BaseIndexedDbService {

  private _currentAppUser = new Subject<AppUser>();

  constructor() {
    super();
  }

  public clean(): void {
    this._currentAppUser.next(null);
  }

  // CRUD operation: GET (single)
  public getAppUser(): Observable<AppUser> {
    super.getAll('appUser').then((value) => {
      this._currentAppUser.next(value[0]);
    });

    return this._currentAppUser.asObservable();
  }

  // CRUD operation: POST - local
  public postAppUser(data: AppUser): void {
    super.postToLocal(data);
  }

  // CRUD operation: POST - sync
  public postAppUserSync(data: AppUser): string | number {
    return super.postToSync(data);
  }

  // CRUD operation: PUT - local
  public putAppUser(data: AppUser): void {
    super.putToLocal(data);
  }

  // CRUD operation: PUT - sync
  public putAppUserSync(data: AppUser): void {
    super.putToSync(data);
  }

  // operation: sync
  public syncAppUser(func: (data: AppUser) => void) {
    this.sync('appUser', func);
  }

  public syncLocalAppUser(appUser: AppUser) {
    this.syncLocal('appUser', [appUser]);
  }

  get currentAppUser(): Subject<AppUser> {
    return this._currentAppUser;
  }
}
