import { TestBed } from '@angular/core/testing';

import { PersonalDataIndexedDbService } from './personal-data.service';

describe('PersonalDataIndexedDbService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PersonalDataIndexedDbService = TestBed.get(PersonalDataIndexedDbService);
    expect(service).toBeTruthy();
  });
});
