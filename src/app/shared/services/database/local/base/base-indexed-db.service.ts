import {AppDatabase} from '../../../../models/app-database';
import {Injectable} from '@angular/core';
import {AppUser} from '../../../../models/app-user.interface';
import {Meal} from '../../../../models/meal.interface';
import {Place} from '../../../../models/place.interface';

@Injectable()
export abstract class BaseIndexedDbService {

  private dataBase: { local: { db: AppDatabase }, sync: { db: AppDatabase } } = {
    local: {db: null},
    sync: {db: null}
  };

  protected constructor() {
    this.dataBase.local.db = new AppDatabase('WorldOfYourMealsDB');
    this.dataBase.sync.db = new AppDatabase('SyncWorldOfYourMealsDB');
  }

  protected postToLocal(item: AppUser | Meal | Place): void {
    this.dataBase.local.db.push(item);
    // console.log('Item has added');
  }

  protected postToSync(item: AppUser | Meal | Place): string | number | null {
    const key = this.dataBase.local.db.push(item);
    // console.log('Item has added to sync db');
    return key;
  }

  protected deleteFromLocal(item: AppUser | Meal | Place): void {
    this.dataBase.local.db.remove(item);
  }

  protected putToLocal(item: AppUser | Meal | Place): void {
    this.dataBase.local.db.put(item);
    // console.log('Put in local db has finished');
  }

  protected putToSync(item: AppUser | Meal | Place): void {
    this.dataBase.sync.db.put(item);
    // console.log('Put in sync db has finished');
  }

  protected async sync(type: 'appUser' | 'meals' | 'places', func: (item: any) => void): Promise<any> {
    const allItems: any[] = await this.dataBase.sync.db.getItems(type);
    this.clean();
    allItems.forEach((item: AppUser | Meal | Place) => {
      this.dataBase.sync.db.remove(item);
      func(item);
    });
    // console.log('Sync has run');
  }

  protected async syncLocal(type: 'appUser' | 'meals' | 'places', itemArray: AppUser[] | Meal[] | Place[]): Promise<any> {
    const allItems: any[] = await this.dataBase.local.db.getItems(type);
    if (type === 'appUser' || allItems.length < itemArray.length) {
      Promise.all(allItems.map(item => {
        return this.dataBase.local.db.remove(item);
      })).then(() => {
        itemArray.forEach((item: AppUser | Meal | Place) => {
          this.dataBase.local.db.push(item);
        });
      });
    }
  }

  protected async getAll(type: 'appUser' | 'meals' | 'places'): Promise<any[]> {
    return this.dataBase.local.db.getItems(type);
  }

  protected async getItem(key: string | number, type: 'appUser' | 'meals' | 'places'): Promise<any> {
    return this.dataBase.local.db.getItem(key, type);
  }


  public abstract clean();
}
