import {async, TestBed} from '@angular/core/testing';

import { BaseIndexedDbService } from './base-indexed-db.service';

describe('BaseIndexedDbService', () => {
  beforeEach(async(() => TestBed.configureTestingModule({
    providers: [
      BaseIndexedDbService
    ]
  })));

  it('should be created', () => {
    const service: BaseIndexedDbService = TestBed.get(BaseIndexedDbService);
    expect(service).toBeTruthy();
  });
});
