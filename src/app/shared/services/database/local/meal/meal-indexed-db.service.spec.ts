import { TestBed } from '@angular/core/testing';

import {MealIndexedDbService} from './meal-indexed-db.service';

describe('MealIndexedDbService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MealIndexedDbService = TestBed.get(MealIndexedDbService);
    expect(service).toBeTruthy();
  });
});
