import {Injectable} from '@angular/core';
import {BaseIndexedDbService} from '../base/base-indexed-db.service';
import {Observable, Subject} from 'rxjs';
import {Meal} from '../../../../models/meal.interface';

@Injectable({
  providedIn: 'root'
})
export class MealIndexedDbService extends BaseIndexedDbService {

  private _currentMeal = new Subject<Meal>();
  private _allMeal = new Subject<Meal[]>();

  constructor() {
    super();
  }

  public clean(): void {
    this._currentMeal.next(null);
    this._allMeal.next([]);
  }

  // CRUD operation: GET (single)
  public getMeal(key: string): Observable<Meal> {
    super.getItem(key, 'meals').then((value) => {
      this._currentMeal.next(value);
    });

    return this._currentMeal.asObservable();
  }

  // CRUD operation: GET (all)
  public getAllMeal(): Observable<Meal[]> {
    super.getAll('meals').then((value) => {
      this._allMeal.next(value);
    });

    return this._allMeal.asObservable();
  }

  // CRUD operation: POST - local
  public postMeal(data: Meal): void {
    super.postToLocal(data);
  }

  // CRUD operation: POST - sync
  public postMealSync(data: Meal): string | number {
    return super.postToSync(data);
  }

  // CRUD operation: PUT - local
  public putMeal(data: Meal): void {
    super.putToLocal(data);
  }

  // CRUD operation: PUT - sync
  public putMealSync(data: Meal): void {
    super.putToSync(data);
  }

  // CRUD operation: DELETE
  public deleteMeal(data: Meal): void {
    super.deleteFromLocal(data);
  }

  public syncMeal(func: (meal: Meal) => void): Promise<any> {
    return this.sync('meals', func);
  }

  public syncLocalMeals(meals: Meal[]) {
    this.syncLocal('meals', meals);
  }

  get currentMeal(): Subject<Meal> {
    return this._currentMeal;
  }

  get allMeal(): Subject<Meal[]> {
    return this._allMeal;
  }
}
