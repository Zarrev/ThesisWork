import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {BaseIndexedDbService} from '../base/base-indexed-db.service';
import {Place} from '../../../../models/place.interface';

@Injectable({
  providedIn: 'root'
})
export class PlaceIndexedDbService extends BaseIndexedDbService {

  private _currentPlace = new Subject<Place>();
  private _allPlace = new Subject<Place[]>();

  constructor() {
    super();
  }

  public clean(): void {
    this._currentPlace.next(null);
    this._allPlace.next(null);
  }

  // CRUD operation: GET (single)
  public getPlace(key: string): Observable<Place> {
    super.getItem(key, 'places').then((value) => {
      this._currentPlace.next(value);
    });

    return this._currentPlace.asObservable();
  }

  // CRUD operation: GET (all)
  public getAllPlace(): Observable<Place[]> {
    super.getAll('places').then((value) => {
      this._allPlace.next(value);
    });

    return this._allPlace.asObservable();
  }

  // CRUD operation: POST - local
  public postPlace(data: Place): void {
    super.postToLocal(data);
  }

  // CRUD operation: POST - sync
  public postPlaceSync(data: Place): string |number {
    return super.postToSync(data);
  }

  // CRUD operation: PUT - local
  public putPlace(data: Place): void {
    super.putToLocal(data);
  }

  // CRUD operation: PUT - sync
  public putPlaceSync(data: Place): void {
    super.putToSync(data);
  }

  // CRUD operation: DELETE
  public deletePlace(data: Place): void {
    super.deleteFromLocal(data);
  }

  public syncPlace(func: (place: Place) => void): Promise<any> {
    return this.sync('places', func);
  }

  public syncLocalPlaces(places: Place[]) {
    this.syncLocal('places', places);
  }

  get currentPlace(): Subject<Place> {
    return this._currentPlace;
  }

  get allPlace(): Subject<Place[]> {
    return this._allPlace;
  }
}
