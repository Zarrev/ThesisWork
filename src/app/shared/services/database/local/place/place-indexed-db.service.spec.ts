import { TestBed } from '@angular/core/testing';

import { PlaceIndexedDbService } from './place-indexed-db.service';

describe('PlaceIndexedDbService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlaceIndexedDbService = TestBed.get(PlaceIndexedDbService);
    expect(service).toBeTruthy();
  });
});
