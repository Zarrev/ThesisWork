/* tslint:disable:no-string-literal */
import {Injectable} from '@angular/core';
import {Marker} from '../../models/place.interface';
import {GeoCoord, HaversineService} from 'ng2-haversine';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {MapInformation} from '../../models/map-information.interface';
import {environment} from '../../../../environments/environment';

const GEOLOCATION_ERRORS = {
  'errors.location.unsupportedBrowser': 'Browser does not support location services',
  'errors.location.permissionDenied': 'You have rejected access to your location',
  'errors.location.positionUnavailable': 'Unable to determine your location',
  'errors.location.timeout': 'Service timeout has been reached'
};


@Injectable()
export class LocationService {

  private myPosition = new Subject<Marker>();
  private mapInformation = new BehaviorSubject<MapInformation>({displayName: null, fullAddress: null});
  private geoReverseService =
    environment.openStreetMaps.nominatimLocationURL +
    environment.openStreetMaps.apiKey +
    '&email="' + environment.developer.email + '"' +
    '&format=json&addressdetails=1&lat={lat}&lon={lon}';
  declined = false;

  constructor(private httpClient: HttpClient) { }

  public getMyPosition(geoLocationOptions?: any): Observable<Marker> {
    geoLocationOptions = geoLocationOptions || {timeout: 5000};
    if (window.navigator && window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
        position => {
          this.myPosition.next({latitude: position.coords.latitude, longitude: position.coords.longitude});
        },
        (error) => {
          switch (error.code) {
            case 1:
              this.myPosition.error(GEOLOCATION_ERRORS['errors.location.permissionDenied']);
              break;
            case 2:
              this.myPosition.error(GEOLOCATION_ERRORS['errors.location.positionUnavailable']);
              break;
            case 3:
              this.myPosition.error(GEOLOCATION_ERRORS['errors.location.timeout']);
              break;
          }
        },
        geoLocationOptions);
    } else {
      this.myPosition.error(GEOLOCATION_ERRORS['errors.location.unsupportedBrowser']);
    }

    return this.myPosition.asObservable();
  }

  get myLocation(): Observable<Marker> {
    return this.myPosition.asObservable();
  }

  get mapInfo(): Observable<MapInformation> {
    return this.mapInformation.asObservable();
  }

  getReverseGeo(marker: Marker): Observable<MapInformation> {
    const service = (this.geoReverseService || '')
      .replace(new RegExp('{lon}', 'ig'), `${marker.longitude}`)
      .replace(new RegExp('{lat}', 'ig'), `${marker.latitude}`);

    return this.httpClient.get(service).pipe(map(data => {
      const val = (data || {});
      const mapInformation: MapInformation = {displayName: null, fullAddress: null};

      mapInformation.displayName = val['display_name'] || '';

      const building = [];
      if (val['address']['building']) {
        building.push(val['address']['building']);
      }
      if (val['address']['mall']) {
        building.push(val['address']['mall']);
      }
      if (val['address']['theatre']) {
        building.push(val['address']['theatre']);
      }

      const zipCity = [];
      if (val['address']['postcode']) {
        zipCity.push(val['address']['postcode']);
      }
      if (val['address']['city']) {
        zipCity.push(val['address']['city']);
      }
      const streetNumber = [];
      if (val['address']['street']) {
        streetNumber.push(val['address']['street']);
      }
      if (val['address']['road']) {
        streetNumber.push(val['address']['road']);
      }
      if (val['address']['footway']) {
        streetNumber.push(val['address']['footway']);
      }
      if (val['address']['pedestrian']) {
        streetNumber.push(val['address']['pedestrian']);
      }
      if (val['address']['house_number']) {
        streetNumber.push(val['address']['house_number']);
      }

      const address = [];
      if (building.length) {
        address.push(building.join(' '));
      }
      if (zipCity.length) {
        address.push(zipCity.join(' '));
      }
      if (streetNumber.length) {
        address.push(streetNumber.join(' '));
      }
      mapInformation.fullAddress = address.join(', ');

      this.mapInformation.next(mapInformation);

      return mapInformation;
    }), catchError(error => {
      return of(error);
      // console.log(error);
    }));
  }
}

