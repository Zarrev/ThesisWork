import {async, TestBed} from '@angular/core/testing';

import { LocationService } from './location.service';
import {HttpClientModule} from '@angular/common/http';
import {HaversineService} from 'ng2-haversine';

describe('LocationService', () => {
  beforeEach(async(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ],
    providers: [
      HaversineService,
      LocationService
    ]
  }).compileComponents()));

  it('should be created', () => {
    const service: LocationService = TestBed.get(LocationService);
    expect(service).toBeTruthy();
  });
});
