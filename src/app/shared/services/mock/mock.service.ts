import {Injectable} from '@angular/core';
import {Meal} from '../../models/meal.interface';
import {Marker, Place} from '../../models/place.interface';
import {AppUser} from '../../models/app-user.interface';
import { MOCK_PLACES, MOCK_MEALS, MOCK_USER } from '../../utils/mocked-datas';

@Injectable({
  providedIn: 'root'
})
export class MockService {

  public MOCK_PLACES = MOCK_PLACES;
  public MOCK_MEALS = MOCK_MEALS;
  public MOCK_USER = MOCK_USER;

  constructor() {
  }

  addMealAndPlace(meal: Meal, place: Place, notExist: boolean) {
    meal.$key = this.MOCK_MEALS.length.toString();
    place.mealKeys.push(meal.$key);
    this.MOCK_MEALS.push(meal);
    if (notExist) {
      place.$key = this.MOCK_PLACES.length.toString();
      this.MOCK_PLACES.push(place);
    }
  }

  getPlaceByName(filter: string): Place {
    const result = this.MOCK_PLACES.filter(x => x.name.toLocaleLowerCase().trim()
      .includes(filter.toLocaleLowerCase().trim()));
    if (result !== null && result.length !== 0) {
      return result[0];
    }
    return null;
  }

  getMealsForPlace(placeKey: string) {
    const place = this.getCurrentPlace(placeKey);
    return this.MOCK_MEALS.filter(x => place.mealKeys.includes(x.$key));
  }

  getCurrentPlace(placeKey: string) {
    return this.MOCK_PLACES.filter(x => x.$key === placeKey)[0];
  }

  removeMeal(placeKey: string, mealKey: string) {
    let del = false;
    this.MOCK_PLACES.forEach(x => {
      if (x.$key === placeKey) {
        let mealRate = 0;
        this.MOCK_MEALS = this.MOCK_MEALS.filter(meal => {
          if (meal.$key === mealKey) {
            mealRate = meal.rate;
          }
          return meal.$key !== mealKey;
        });
        x.avgRate = this.calcAvgRate(mealRate, x.mealKeys.length, x.avgRate, -1);
        x.mealKeys = x.mealKeys.filter(mKey => mKey !== mealKey);
        del = x.mealKeys.length === 0;
      }
    });

    if (del) {
      this.MOCK_PLACES = this.MOCK_PLACES.filter(x => x.$key !== placeKey);
    }
  }

  calcAvgRate(newMealRate: number, mealKeysLength: number, actualAvgValue: number, addingOrRemoving: 1 | -1 = 1): number {
    return ((actualAvgValue * mealKeysLength) + (newMealRate * addingOrRemoving)) / (mealKeysLength + addingOrRemoving);
  }

  getMarkers() {
    const markers: Marker[] = [];
    this.MOCK_PLACES.forEach(x => markers.push(x.marker));
    return markers;
  }

  setUser(user: AppUser) {
    this.MOCK_USER = user;
  }
}
