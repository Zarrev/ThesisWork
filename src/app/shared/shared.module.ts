import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AngularOpenlayersModule} from 'ngx-openlayers';
import {MapComponent} from './components/map/map.component';
import {HaversineService} from 'ng2-haversine';
import {LocationService} from './services/location/location.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatTabsModule} from '@angular/material/tabs';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSnackBarModule} from '@angular/material/';
import {MatNativeDateModule} from '@angular/material/';
import {OverlayModule} from '@angular/cdk/overlay';
import {RouterModule} from '@angular/router';
import {ChartModule} from 'primeng/chart';
import {RatingModule} from 'ngx-bootstrap';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireModule} from '@angular/fire';
import {LoadingService} from './services/loading/loading.service';
import {LoadingComponent} from './components/loading/loading.component';

@NgModule({
  declarations: [
    MapComponent,
    LoadingComponent
  ],
  imports: [
    CommonModule,
    AngularOpenlayersModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatTabsModule,
    MatIconModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatListModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    RatingModule.forRoot(),
    OverlayModule,
    ChartModule,
    RouterModule,
    MatSnackBarModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule
  ],
  exports: [
    MapComponent,
    LoadingComponent,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatTabsModule,
    MatIconModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatListModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    OverlayModule,
    ChartModule,
    RouterModule,
    MatSnackBarModule,
    AngularFireModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [HaversineService, LocationService, LoadingService]
    };
  }
}
