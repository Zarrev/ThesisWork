import {ChangeDetectorRef} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';

export class MobileQueryUtil {

  private _mobileQuery: MediaQueryList;
  private mobileQueryListener: () => any;

  constructor(public changeDetectorRef: ChangeDetectorRef, public media: MediaMatcher) {

  }

  start() {
    this._mobileQuery = this.media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
    this._mobileQuery.addListener(this.mobileQueryListener);
  }

  removeEvent() {
    this._mobileQuery.removeListener(this.mobileQueryListener);
  }

  get mobileQuery() {
    return this._mobileQuery;
  }
}
