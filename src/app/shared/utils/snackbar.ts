import {MatSnackBar} from '@angular/material';

export function openSnackBar(snackbar: MatSnackBar, message: string) {
  snackbar.open(message, 'Ok', {
    duration: 2000,
  });
}
