import {AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {Feature, geom, layer, Overlay, proj, source, style} from 'openlayers';
import {Marker, Place} from '../../models/place.interface';
import {MapComponent as Map} from 'ngx-openlayers';
import {MapInformation} from '../../models/map-information.interface';
import {ActivatedRoute} from '@angular/router';
import {LocationService} from '../../services/location/location.service';
import {openSnackBar} from '../../utils/snackbar';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  providers: [LocationService]
})
export class MapComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('map', {static: true}) map: Map;

  @Input() width = '100%';
  @Input() height = '300px';
  @Input() latitudePointer = 47.49645470050308;
  @Input() longitudePointer = 19.069904183420064;
  @Input() showControlsZoom: boolean;
  @Input() showControlsCurrentLocation: boolean;
  @Input() showDebugInfo: boolean;
  @Input() zoom = 18;
  @Input() places: Place[] = [];
  @Input() multiMarkerMode = false;
  @Input() markerSize = 40;

  @Output() marker = new EventEmitter<Marker>();
  @Output() addressChanged = new EventEmitter<string>();

  centerLat = 47.49645470050308;
  centerLng = 19.069904183420064;
  markerImage = '../../../assets/img/location-pin.svg';
  pointedAddress: string;
  pointedAddressOrg: string;
  position: Marker = {latitude: this.latitudePointer, longitude: this.longitudePointer};
  placeContext: Place = null;
  clusterNumber = 0;

  private subscriptions: Subscription[] = [];
  private firstTimeRun;


  @Input() routerLinkFunction: (place: Place) => string[] | false = (place: Place) => {
    return [''];
  };

  constructor(private geoLocationService: LocationService, private ref: ChangeDetectorRef,
              private route: ActivatedRoute, private snackbar: MatSnackBar) {
  }

  ngOnInit() {
    this.firstTimeRun = true;
    this.subscriptions.push(this.geoLocationService.getMyPosition().subscribe(position => {
    }, error => openSnackBar(this.snackbar, error)));
    if (!this.isGeolocationDeclined) {
      this.subscriptions.push(this.geoLocationService.myLocation.subscribe(position => {
        this.latitudePointer = position.latitude;
        this.longitudePointer = position.longitude;
        if (this.firstTimeRun) {
          this.firstTimeRun = false;
          this.position = {latitude: position.latitude, longitude: position.longitude};
          this.searchForPositionAddress();
        }
      }, error => openSnackBar(this.snackbar, error)));
    } else {
      this.searchForPositionAddress();
    }
    this.searchForPositionAddress();
    this.subscriptions.push(this.geoLocationService.mapInfo.subscribe(data => {
      if (data === null) {
        return;
      }
      this.pointedAddressOrg = data.displayName;
      this.pointedAddress = data.fullAddress;
      if (this.showControlsCurrentLocation) {
        this.addressChanged.emit(this.pointedAddress);
      }
    }));
    this.marker.emit(this.position);
  }

  ngAfterViewInit(): void {
    const requestedLng = Number(this.route.snapshot.paramMap.get('longitude'));
    const requestedLtd = Number(this.route.snapshot.paramMap.get('latitude'));
    setTimeout(() => {
      this.map.instance.updateSize();
      if (requestedLng && requestedLtd) {
        this.setCenterOfMap(requestedLng, requestedLtd);
      } else {
        if (this.places.length === 0) {
          this.setCenterOfMap(this.position.longitude, this.position.latitude);
        } else {
          this.fitMapForMarkers();
        }
      }
      this.addMarkersToMap();
      this.ref.markForCheck();
    }, 0);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(x => x.unsubscribe());
  }

  private closePopUp(overlay: Overlay, popup: HTMLElement) {
    overlay.setPosition(undefined);
    popup.blur();
  }

  private addMarkersToMap() {
    const features: Feature[] = [];
    this.places.forEach(x => {
      const feature = new Feature({
        geometry: new geom.Point(proj.fromLonLat([x.marker.longitude, x.marker.latitude])),
      });
      feature.setId(x.$key);
      feature.setStyle(new style.Style({
        image: new style.Icon({
          anchor: [0.5, 1],
          anchorXUnits: 'fraction',
          anchorYUnits: 'fraction',
          anchorOrigin: 'top-left',
          scale: 0.1,
          src: this.markerImage
        })
      }));
      features.push(feature);
    });
    const featureSource = new source.Vector({features});
    const clusterSource = new source.Cluster({
      distance: 30,
      source: featureSource
    });
    let styleCache = {};
    const myLayer = new layer.Vector({
      source: clusterSource,
      style: (feature) => {
        const size = feature.get('features').length;
        let stylee = styleCache[size];
        if (!stylee) {
          stylee = new style.Style({
            image: new style.Icon({
              anchor: [0.5, 1],
              anchorXUnits: 'fraction',
              anchorYUnits: 'fraction',
              anchorOrigin: 'top-left',
              scale: 0.1,
              src: this.markerImage
            }),
          });
          styleCache[size] = stylee;
        }
        return stylee;
      }
    });
    this.map.instance.addLayer(myLayer);
  }

  setCenterOfMap(longitude: number, latitude: number) {
    this.centerLng = longitude;
    this.centerLat = latitude;
  }

  onSingleClick(event) {
    if (this.places.length === 0) {
      const lonlat = proj.transform(event.coordinate, 'EPSG:3857', 'EPSG:4326');
      this.position.longitude = lonlat[0];
      this.position.latitude = lonlat[1];
      this.marker.emit(this.position);
      this.searchForPositionAddress();
    } else {
      this.placeContext = null;
      this.addMarkerLayerWithPopUp(event);
    }
  }

  increaseZoom() {
    if (this.zoom < 18) {
      this.zoom++;
    }
  }

  decreaseZoom() {
    if (this.zoom > 0) {
      this.zoom--;
    }
  }

  setCurrentLocation() {
    if (this.latitudePointer && this.longitudePointer) {
      this.position = {latitude: this.latitudePointer, longitude: this.longitudePointer};
      this.setCenterOfMap(this.position.longitude, this.position.latitude);
      this.zoom = 15;
      this.searchForPositionAddress();
    }
  }

  searchForPositionAddress() {
    this.searchForAddress(this.position);
  }

  searchForAddress(marker: Marker) {
    this.subscriptions.push(this.geoLocationService.getReverseGeo(marker).subscribe(() => {
    }, error => openSnackBar(this.snackbar, error)));
  }

  getAddressForMarker(): Observable<MapInformation> {
    return this.geoLocationService.mapInfo;
  }

  fitMapForMarkers() {
    let minlong = this.places[0].marker.longitude;
    let maxlong = this.places[0].marker.longitude;
    let minlat = this.places[0].marker.latitude;
    let maxlat = this.places[0].marker.latitude;
    for (const place of this.places) {
      const marker = place.marker;
      if (marker.latitude < minlat) {
        minlat = marker.latitude;
      }
      if (marker.latitude > maxlat) {
        maxlat = marker.latitude;
      }

      if (marker.longitude < minlong) {
        minlong = marker.longitude;
      }
      if (marker.longitude > maxlong) {
        maxlong = marker.longitude;
      }

    }
    this.setCenterOfMap((minlong + maxlong) / 2, (minlat + maxlat) / 2);
    if (this.places.length > 1) {
      let extent: [number, number, number, number] = [minlong, minlat, maxlong, maxlat];
      if (this.map.instance) {
        extent = proj.transformExtent(extent, 'EPSG:4326', 'EPSG:3857');
        this.map.instance.getView().fit(extent, {size: this.map.instance.getSize(), padding: [170, 50, 30, 150]});
      }
    }
  }

  private isCluster(feature): boolean {
    if (!feature || !feature.get('features')) {
      return false;
    }
    return feature.get('features').length > 1;
  }

  addMarkerLayerWithPopUp(event) {
    const container = document.getElementById('popup');

    const overlay = new Overlay({
      element: container,
      autoPan: true
    });
    this.map.instance.addOverlay(overlay);
    if (this.map.instance.hasFeatureAtPixel(event.pixel) === true) {
      const coordinate = event.coordinate;
      this.map.instance.forEachFeatureAtPixel(event.pixel, (feature => {
        if (!this.isCluster(feature)) {
          feature = feature.get('features')[0];
          this.placeContext = this.places.filter(x => x.$key === (feature as Feature).getId())[0];
          this.searchForAddress(this.placeContext.marker);
          this.setCenterOfMap(this.placeContext.marker.longitude, this.placeContext.marker.latitude);
          overlay.setPosition(coordinate);
        } else {
          this.clusterNumber = feature.get('features').length;
          overlay.setPosition(coordinate);
        }
      }));
    } else {
      this.closePopUp(overlay, container);
    }
  }

  get isGeolocationDeclined(): boolean {
    return this.geoLocationService.declined;
  }
}
