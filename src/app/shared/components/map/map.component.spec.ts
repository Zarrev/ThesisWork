import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MapComponent} from './map.component';
import {LocationService} from '../../services/location/location.service';
import {MatButtonModule, MatIconModule, MatSnackBar, MatSnackBarModule} from '@angular/material';
import {RatingModule} from 'ngx-bootstrap';
import {AngularOpenlayersModule} from 'ngx-openlayers';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MapComponent],
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        AngularOpenlayersModule,
        MatIconModule,
        MatButtonModule,
        MatSnackBarModule,
        RatingModule.forRoot(),
        RouterTestingModule
      ],
      providers: [
        LocationService,
        MatSnackBar
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
