import Dexie from 'dexie';
import {AppUser} from './app-user.interface';
import {Meal} from './meal.interface';
import {Place} from './place.interface';
import {AppUserLocal} from './local-db-models/app-user.localdb.interface';
import {MealLocal} from './local-db-models/meal.localdb.interface';
import {PlaceLocal} from './local-db-models/place.localdb.interface';
import * as uuid from 'uuid';

export class AppDatabase extends Dexie {

  readonly appUserTableName = 'appUser';
  readonly mealsTableName = 'meals';
  readonly placesTableName = 'places';

  constructor(public databaseName: string) {
    super(databaseName);
    this.version(1).stores({
      appUser: '$key, nickname, profilePicture, notifyNumber, gps, placeKeys',
      meals: '$key, picture, name, rate, date',
      places: '$key, mealKeys, latitude, longitude, name, avgRate',
    });
  }

  push(item: AppUser | Meal | Place): string | number | null {
    const tableWithItem = this.getNeededTableByObject(item);
    if (!tableWithItem) {
      return null;
    }
    this.table(tableWithItem.table).add(tableWithItem.item);
    return tableWithItem.item.$key;
  }

  put(item: AppUser | Meal | Place): Promise<any> {
    const tableWithItem = this.getNeededTableByObject(item);
    if (!tableWithItem) {
      return new Promise((resolve, reject) => {
        resolve(null);
      });
    }
    return this.table(tableWithItem.table).update(tableWithItem.item.$key, tableWithItem.item);
  }

  remove(item: AppUser | Meal | Place): Promise<any> {
    const tableWithItem = this.getNeededTableByObject(item);
    if (!tableWithItem) {
      return new Promise((resolve, reject) => {
        resolve(null);
      });
    }
    return this.table(tableWithItem.table).delete(tableWithItem.item.$key);
  }

  getItems(type: 'appUser' | 'meals' | 'places'): Dexie.Promise<any[]> {
    const table = this.getNeededTableByType(type);
    return this.table(table).toArray();
  }

  getItem(key: string | number, type: 'appUser' | 'meals' | 'places'): Dexie.Promise<any> {
    const table = this.getNeededTableByType(type);
    return this.table(table).get(key);
  }

  private instanceOfAppUser(item: any): boolean {
    return 'settings' in item && 'fireUser' in item;
  }

  private instanceOfMeal(item: any): boolean {
    return 'picture' in item && 'name' in item && 'date' in item && 'rate' in item;
  }

  private instanceOfPlace(item: any): boolean {
    return 'mealKeys' in item && 'name' in item && 'avgRate' in item;
  }

  private getNeededTableByObject(item: AppUser | Meal | Place): {table: string, item: AppUserLocal | MealLocal | PlaceLocal} | null {
    if (this.instanceOfAppUser(item)) {
      return {table: this.appUserTableName, item: this.convertToAppUserLocal(item as AppUser)};
    } else if (this.instanceOfMeal(item)) {
      return {table: this.mealsTableName, item: this.convertToMealLocal(item as Meal)};
    } else if (this.instanceOfPlace(item)) {
      return {table: this.placesTableName, item: this.convertToPlaceLocal(item as Place)};
    }

    return null;
  }

  private getNeededTableByType(type: 'appUser' | 'meals' | 'places'): string {
    if (type === this.appUserTableName) {
      return this.appUserTableName;
    } else if (type === this.mealsTableName) {
      return this.mealsTableName;
    }
    return this.placesTableName;
  }

  convertToAppUserLocal(item: AppUser): AppUserLocal {
    return {
      $key: item.fireUser.uid,
      nickname: item.profile.nickname,
      profilePicture: item.profile.profilePicture,
      notifyNumber: item.settings.notifyNumber,
      gps: item.settings.gps,
      placeKeys: item.placeKeys
    };
  }

  convertToMealLocal(item: Meal): MealLocal {
    return {
      $key: item.$key ? item.$key : uuid.v4(),
      picture: item.picture,
      name: item.name,
      rate: item.rate,
      date: item.date
    };
  }

  convertToPlaceLocal(item: Place): PlaceLocal {
    return {
      $key: item.$key,
      mealKeys: item.mealKeys,
      latitude: item.marker.latitude,
      longitude: item.marker.longitude,
      name: item.name,
      avgRate: item.avgRate
    };
  }
}
