import {Marker} from '../place.interface';

export interface Message {
  timestamp: number;
  senderId: string;
  name: string;
  email: string;
  picture: string;
  payload: Payload;
}

export interface Payload {
  placeName: string;
  avgRate: number;
  marker: Marker;
  meal: {
    picture: string;
    name: string;
    rate: number;
  };
  text?: string;
}
