export interface Users {
  key: string;
  email: string;
  fcmToken?: string;
}
