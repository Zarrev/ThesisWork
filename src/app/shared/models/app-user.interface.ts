import {User as FireUser} from 'firebase/app';

export interface AppUser {
  fireUser: FireUser;
  settings: Settings;
  profile?: Profile;
  placeKeys?: string[];
}

export interface Profile {
  nickname?: string;
  profilePicture?: string;
}

export interface Settings {
  notifyNumber: number;
  gps: boolean;
}

