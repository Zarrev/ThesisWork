export interface MapInformation {
  displayName: string;
  fullAddress: string;
}
