export interface AppUserLocal {
  $key: string | number;
  nickname?: string;
  profilePicture?: string;
  notifyNumber: number;
  gps: boolean;
  placeKeys: string[];
}
