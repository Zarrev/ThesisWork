export interface PlaceLocal {
  $key: string | number;
  mealKeys?: string[] | number[];
  latitude?: number;
  longitude?: number;
  name: string;
  avgRate: number;
}
