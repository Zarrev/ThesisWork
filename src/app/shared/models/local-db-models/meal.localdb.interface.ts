export interface MealLocal {
  $key: string | number;
  picture?: string;
  name: string;
  rate: number;
  date: number;
}
