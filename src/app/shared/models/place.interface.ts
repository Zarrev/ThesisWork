export interface Place {
  $key: string;
  mealKeys: string[];
  marker?: Marker;
  name: string;
  avgRate: number;
}

export interface Marker {
  latitude: number;
  longitude: number;
}
