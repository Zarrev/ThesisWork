export interface Meal {
  $key: string;
  picture: string;
  name: string;
  rate: number;
  date: number;
}
