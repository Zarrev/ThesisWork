import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CorePageComponent} from './pages/core-page/core-page.component';
import {MainComponent} from './components/main/main.component';
import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/auth-guard';

const redirectUnauthorizedToMain = () => redirectUnauthorizedTo(['main']);

const routes: Routes = [
  {
    path: '',
    component: CorePageComponent,
    children: [
      {
        path: '',
        redirectTo: 'main',
        pathMatch: 'full'
      },
      {
        path: 'main',
        component: MainComponent
      },
      {
        path: 'register_food',
        loadChildren: () => import('../features/regist/regist.module').then(m => m.RegistModule),
        canActivate: [AngularFireAuthGuard],
        data: { authGuardPipe: redirectUnauthorizedToMain }
      },
      {
        path: 'history',
        loadChildren: () => import('../features/history/history.module').then(m => m.HistoryModule),
        canActivate: [AngularFireAuthGuard],
        data: { authGuardPipe: redirectUnauthorizedToMain }
      },
      {
        path: 'profile',
        loadChildren: () => import('../features/profile/profile.module').then(m => m.ProfileModule),
        canActivate: [AngularFireAuthGuard],
        data: { authGuardPipe: redirectUnauthorizedToMain }
      },
      {
        path: 'community',
        loadChildren: () => import('../features/community/community.module').then(m => m.CommunityModule),
        canActivate: [AngularFireAuthGuard],
        data: { authGuardPipe: redirectUnauthorizedToMain }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
