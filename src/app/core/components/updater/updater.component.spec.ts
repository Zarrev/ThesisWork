import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UpdaterComponent} from './updater.component';
import {MatBadgeModule, MatSidenavModule, MatToolbarModule} from '@angular/material';
import {ModalModule} from 'ngx-bootstrap';
import {ServiceWorkerModule, SwUpdate} from '@angular/service-worker';
import {SharedModule} from '../../../shared/shared.module';
import {UpdateService} from '../../services/update/update.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('UpdaterComponent', () => {
  let component: UpdaterComponent;
  let fixture: ComponentFixture<UpdaterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpdaterComponent],
      imports: [
        ModalModule.forRoot(),
        SharedModule,
        BrowserAnimationsModule,
        ServiceWorkerModule.register('', {enabled: false}),
      ],
      providers: [
        UpdateService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
