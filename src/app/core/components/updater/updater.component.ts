import {Component, EventEmitter, OnInit, Output, TemplateRef} from '@angular/core';
import {SwUpdate} from '@angular/service-worker';
import {UpdateService} from '../../services/update/update.service';
import Dexie from 'dexie';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-updater',
  templateUrl: './updater.component.html',
  styleUrls: ['./updater.component.scss']
})
export class UpdaterComponent implements OnInit {

  modalRef: BsModalRef;
  update = false;

  private config = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false,
    class: 'update-modal'
  };

  @Output() available = new EventEmitter<boolean>();

  constructor(private updateService: UpdateService, private swUpdate: SwUpdate, private modalService: BsModalService) {
    this.swUpdate.available.subscribe((event) => {
      this.update = true;
      this.available.emit(this.update);
    });
  }

  ngOnInit() {
    this.available.emit(this.update);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  updateAndClose(): void {
    this.swUpdate.activateUpdate().then(() => {
      this.modalRef.hide();
      Dexie.delete('MealLocalDB');
      document.location.reload();
    });
  }

  // import * as firebase from 'firebase';
  //
  // async askForPermissioToReceiveNotifications() {
  //   // ezzel a függvénnyel lehet megkapni a push notihoz a tokent
  //   try {
  //     const messaging = firebase.messaging();
  //     await messaging.requestPermission();
  //     const token = await messaging.getToken();
  //     console.log('Token: ', token);
  //
  //     return token;
  //   } catch (error) {
  //     console.error(error);
  //   }
  // }
}
