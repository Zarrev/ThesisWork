import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {PersonalDataService} from '../../../shared/services/object-handler/app-user/personal-data.service';
import {NetworkService} from '../../../shared/services/network/network.service';
import {MatSnackBar} from '@angular/material';
import {openSnackBar} from '../../../shared/utils/snackbar';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {

  isOnline: boolean;
  contactEmail: string;
  private _actionButtonLabel = 'Log In';
  private subscs: Subscription[] = [];
  public click: () => void;

  constructor(private authenticationService: PersonalDataService, private router: Router,
              private networkService: NetworkService, private _matSnackBar: MatSnackBar) {
    this.contactEmail = environment.developer.secondaryEmail;
    this.isOnline = this.networkService.isOnline;
    this.subscs.push(this.networkService.connectionChanged.subscribe(value => this.isOnline = value));
    this.subscs.push(this.authenticationService.isLoggedIn.subscribe(loggedIn => {
      if (loggedIn) {
        this._actionButtonLabel = 'Add my meal!';
        this.click = () => {
          if (this.isOnline) {
            this.router.navigate(['/register_food']);
          }
        };
      } else {
        this._actionButtonLabel = 'Log In';
        this.click = () => {
          if (this.isOnline) {
            this.authenticationService.signIn();
          }
        };
      }
    }));
  }

  ngOnInit() {
  }

  get actionButtonLabel() {
    return this._actionButtonLabel;
  }

  ngOnDestroy(): void {
    this.subscs.forEach(x => x.unsubscribe());
  }

  showPopUp() {
    if (!this.isOnline) {
      openSnackBar(this._matSnackBar, 'Please connect to the internet to reach this function.');
    }
  }
}
