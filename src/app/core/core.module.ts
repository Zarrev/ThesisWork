import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './components/main/main.component';
import {CoreRoutingModule} from './core-routing.module';
import { CorePageComponent } from './pages/core-page/core-page.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import { LoadingComponent } from '../shared/components/loading/loading.component';
import {LoadingService} from '../shared/services/loading/loading.service';
import {SharedModule} from '../shared/shared.module';
import {ServiceWorkerModule} from '@angular/service-worker';
import { UpdaterComponent } from './components/updater/updater.component';
import {UpdateService} from './services/update/update.service';
import {MatBadgeModule} from '@angular/material';
import {ModalModule} from 'ngx-bootstrap';
import {AngularFireAuthGuardModule} from '@angular/fire/auth-guard';
import {HungryModule} from '../features/hungry/hungry.module';

@NgModule({
  declarations: [
    MainComponent,
    CorePageComponent,
    UpdaterComponent
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    MatToolbarModule,
    MatSidenavModule,
    MatBadgeModule,
    ModalModule.forRoot(),
    ServiceWorkerModule,
    SharedModule.forRoot(),
    AngularFireAuthGuardModule,
    HungryModule.forRoot()
  ],
  providers: [
    UpdateService
  ],
  entryComponents: [
    LoadingComponent
  ]
})
export class CoreModule { }
