import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorePageComponent } from './core-page.component';
import {MatBadgeModule, MatSidenavModule, MatToolbarModule} from '@angular/material';
import {ModalModule} from 'ngx-bootstrap';
import {SharedModule} from '../../../shared/shared.module';
import {ServiceWorkerModule} from '@angular/service-worker';
import {HungryModule} from '../../../features/hungry/hungry.module';
import {LoadingService} from '../../../shared/services/loading/loading.service';
import {UpdateService} from '../../services/update/update.service';
import {UpdaterComponent} from '../../components/updater/updater.component';
import {RouterTestingModule} from '@angular/router/testing';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('CorePageComponent', () => {
  let component: CorePageComponent;
  let fixture: ComponentFixture<CorePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorePageComponent, UpdaterComponent ],
      imports: [
        BrowserAnimationsModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        RouterTestingModule,
        MatToolbarModule,
        MatSidenavModule,
        MatBadgeModule,
        ModalModule.forRoot(),
        SharedModule.forRoot(),
        ServiceWorkerModule.register('', {enabled: false}),
        HungryModule.forRoot()
      ],
      providers: [
        LoadingService,
        UpdateService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
