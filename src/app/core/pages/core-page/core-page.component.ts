import {AfterContentChecked, AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {MatSidenav} from '@angular/material';
import {MobileQueryUtil} from '../../../shared/utils/mobile-query.util';
import {MediaMatcher} from '@angular/cdk/layout';
import {NavigationCancel, NavigationEnd, NavigationStart, Router} from '@angular/router';
import {LoadingService} from '../../../shared/services/loading/loading.service';
import {PersonalDataService} from '../../../shared/services/object-handler/app-user/personal-data.service';
import {NearNotifierService} from '../../../features/hungry/services/push-noti/near-notifier.service';
import {NetworkService} from '../../../shared/services/network/network.service';

@Component({
  selector: 'app-core-page',
  templateUrl: './core-page.component.html',
  styleUrls: ['./core-page.component.scss']
})
export class CorePageComponent implements OnInit, OnDestroy, AfterContentChecked, AfterViewInit {

  @ViewChild('sideNav', { static: false }) sidenav: MatSidenav;

  updateAvailable = false;
  isOnline: boolean;

  private _loggedIn = false;
  private subscs: Subscription[] = [];
  private mobileQueryUtil: MobileQueryUtil;

  constructor(private router: Router, private loadingService: LoadingService, private networkService: NetworkService,
              private personalDataService: PersonalDataService, private nearNotifierService: NearNotifierService,
              changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.isOnline = this.networkService.isOnline;
    this.subscs.push(this.networkService.connectionChanged.subscribe(value => this.isOnline = value));
    this.subscs.push(this.personalDataService.isLoggedIn.subscribe(loggedIn => this._loggedIn = loggedIn));
    this.mobileQueryUtil = new MobileQueryUtil(changeDetectorRef, media);
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.subscs.push(this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationStart) {
          this.loadingService.show();
        } else if (event instanceof NavigationEnd || event instanceof NavigationCancel) {
          this.loadingService.hide();
        }
      }));
  }

  ngAfterContentChecked() {
    this.mobileQueryUtil.start();
  }

  ngOnDestroy(): void {
    this.mobileQueryUtil.removeEvent();
    this.subscs.forEach(x => x.unsubscribe());
  }

  logOut() {
    this.closeSideNav();
    this.personalDataService.signOut();
  }

  closeSideNav() {
    this.sidenav.close();
  }

  hungry() {
    this.nearNotifierService.sendNotification();
    this.closeSideNav();
  }

  setUpdateAvailable(eventValue: boolean) {
    this.updateAvailable = eventValue;
  }

  get mobileQuery() {
    return this.mobileQueryUtil.mobileQuery;
  }

  get loggedIn() {
    return this._loggedIn;
  }

}
