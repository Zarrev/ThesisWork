import {async, TestBed} from '@angular/core/testing';

import { UpdateService } from './update.service';
import {ServiceWorkerModule} from '@angular/service-worker';

describe('UpdateServiceService', () => {
  beforeEach(async(() => TestBed.configureTestingModule({
    imports: [
      ServiceWorkerModule.register('', {enabled: false}),
    ],
    providers: [
      UpdateService
    ]
  })));

  it('should be created', () => {
    const service: UpdateService = TestBed.get(UpdateService);
    expect(service).toBeTruthy();
  });
});
