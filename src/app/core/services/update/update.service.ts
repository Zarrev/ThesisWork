import {ApplicationRef, Injectable} from '@angular/core';
import {SwUpdate, SwPush} from '@angular/service-worker';
import {first} from 'rxjs/operators';
import {concat, interval} from 'rxjs';

@Injectable()
export class UpdateService {

  constructor(appRef: ApplicationRef, updates: SwUpdate, push: SwPush) {
    // Allow the app to stabilize first, before starting polling for updates with `interval()`.
    const appIsStable = appRef.isStable.pipe(first(isStable => isStable === true));
    // Wait 6 hours for the next update search : h * m * s * ms =  6 * 60 * 60 * 1000 = 6h
    const everySixHours = interval( 60 * 1000);
    const everySixHoursOnceAppIsStable = concat(appIsStable, everySixHours);
    everySixHoursOnceAppIsStable.subscribe(() => {
      updates.checkForUpdate();
      console.log('UpdateService has checked for updates of service worker.');
    });
  }
}
