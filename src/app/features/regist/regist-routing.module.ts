import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RegistPageComponent} from './pages/regist-page/regist-page.component';

const routes: Routes = [
  {
    path: '',
    component: RegistPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistRoutingModule { }
