import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {Marker} from '../../../../shared/models/place.interface';
import {LocationService} from '../../../../shared/services/location/location.service';
import {When} from '../../models/when.enum';
import {openSnackBar} from '../../../../shared/utils/snackbar';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-place-form',
  templateUrl: './place-form.component.html',
  styleUrls: ['./place-form.component.scss']
})
export class PlaceFormComponent implements OnInit, OnDestroy {

  errorMsg = {
    namePlace: 'The place\'s name is required field!',
    address: 'You have to pin the place on the map!'
  };
  placeFormGroup: FormGroup;
  private subsc: Subscription[] = [];
  private marker: Marker = null;
  @Input() mobileView = true;
  @Output() earlier = new EventEmitter<boolean>();
  @Output() selectedMarker = new EventEmitter<Marker>();

  constructor(private formBuilder: FormBuilder, private locationService: LocationService, private snackbar: MatSnackBar) {
    this.placeFormGroup = this.formBuilder.group({
      when: [When.Now, [Validators.required, Validators.nullValidator]],
      name: ['', [Validators.required, Validators.nullValidator]],
      address: ['', [Validators.required, Validators.nullValidator]],
    });
    this.subsc.push(this.locationService.getMyPosition().subscribe(value => {
    }, error => {
      openSnackBar(this.snackbar, error);
    }));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subsc.forEach(x => x.unsubscribe());
  }

  instanceOfMarker(object: any): boolean {
    return 'latitude' in object && 'longitude' in object;
  }

  onMarkerChange(event: Marker): void {
    const marker: Marker = {latitude: event.latitude, longitude: event.longitude};
    this.marker = this.instanceOfMarker(marker) ? marker : null;
  }

  onAddressChange(event: string): void {
    this.placeFormGroup.patchValue({
      address: event
    });
  }

  sendValues() {
    this.earlier.emit(this.placeFormGroup.controls.when.value === When.Earlier);
    this.selectedMarker.emit(this.marker);
  }

  get getMarker(): Marker {
    return this.marker;
  }

  get whenNow(): string {
    return When.Now;
  }

  get whenEarlier(): string {
    return When.Earlier;
  }
}
