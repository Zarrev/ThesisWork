import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceFormComponent } from './place-form.component';
import {SharedModule} from '../../../../shared/shared.module';
import {CdkStepper, CdkStepperModule, STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {MatStepperModule} from '@angular/material';
import {ChangeDetectorRef, ElementRef} from '@angular/core';
import {MockElementRef} from '../../../../../test_utils/element-ref.mock';

describe('PlaceFormComponent', () => {
  let component: PlaceFormComponent;
  let fixture: ComponentFixture<PlaceFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceFormComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        CdkStepperModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        SharedModule.forRoot(),
      ],
      providers: [
        { provide: ElementRef, useClass: MockElementRef },
        ChangeDetectorRef,
        CdkStepper,
        {
          provide: STEPPER_GLOBAL_OPTIONS,
          useValue: {showError: true }
        }
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
