import {Component, Input, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ImageSnippet} from '../../models/image-snippet.class';
import {NgxImageCompressService} from 'ngx-image-compress';

@Component({
  selector: 'app-meal-form',
  templateUrl: './meal-form.component.html',
  styleUrls: ['./meal-form.component.scss']
})
export class MealFormComponent implements OnInit {

  @Input() dateIsNeeded: boolean;
  errorMsg = {
    picture: 'You have to upload an image!',
    name: 'The meal\'s name is required field!',
    rate: 'The rate is required field!',
    dateReq: 'Select a date please!',
    dateFuture: 'The selected date is in the future!'
  };
  mealFormGroup: FormGroup;
  selectedFile = new ImageSnippet('/assets/img/food_pic.svg', null);
  today = new Date();
  private subsc: Subscription[] = [];
  private validateFuture = (group: FormGroup) => {
    const datefield = group.get('date');
    return datefield && datefield.value <= new Date() ? null : {feature: true};
  };

  constructor(private formBuilder: FormBuilder, private imageCompress: NgxImageCompressService) {
    this.mealFormGroup = this.formBuilder.group({
      picture: [null, Validators.required],
      name: ['', Validators.required],
      rate: [1, [Validators.required, Validators.max(5), Validators.min(1)]],
      date: [{value: new Date(), disabled: true}, [Validators.required, this.validateFuture]]
    });
  }

  ngOnInit() {
  }

  processFile() {
    this.imageCompress.uploadFile().then(({image, orientation}) => {
      this.imageCompress.compressFile(image, orientation, 50, 50).then(
        result => {
          this.mealFormGroup.patchValue({
            picture: result
          });
          this.selectedFile = new ImageSnippet(result, null);
        }
      );

    });
  }
}
