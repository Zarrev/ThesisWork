import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealFormComponent } from './meal-form.component';
import {SharedModule} from '../../../../shared/shared.module';
import {RatingModule} from 'ngx-bootstrap';
import {CdkStepper, CdkStepperModule, STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {MatStepperModule} from '@angular/material';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ChangeDetectorRef, ElementRef} from '@angular/core';
import {MockElementRef} from '../../../../../test_utils/element-ref.mock';
import {NgxImageCompressService} from 'ngx-image-compress';

describe('MealFormComponent', () => {
  let component: MealFormComponent;
  let fixture: ComponentFixture<MealFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ MealFormComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        CdkStepperModule,
        RouterTestingModule,
        MatStepperModule,
        SharedModule.forRoot(),
        RatingModule.forRoot(),
        BrowserAnimationsModule
      ],
      providers: [
        { provide: ElementRef, useClass: MockElementRef },
        ChangeDetectorRef,
        CdkStepper,
        {
          provide: STEPPER_GLOBAL_OPTIONS,
          useValue: {showError: true }
        },
        NgxImageCompressService
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MealFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
