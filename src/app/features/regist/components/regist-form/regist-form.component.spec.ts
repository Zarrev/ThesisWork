import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistFormComponent } from './regist-form.component';
import {SharedModule} from '../../../../shared/shared.module';
import {RatingModule} from 'ngx-bootstrap';
import {CdkStepperModule, STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {PlaceFormComponent} from '../place-form/place-form.component';
import {MealFormComponent} from '../meal-form/meal-form.component';
import {MatStepperModule} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxImageCompressService} from 'ngx-image-compress';

describe('RegistFormComponent', () => {
  let component: RegistFormComponent;
  let fixture: ComponentFixture<RegistFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistFormComponent, PlaceFormComponent, MealFormComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        CdkStepperModule,
        RouterTestingModule,
        MatStepperModule,
        SharedModule.forRoot(),
        RatingModule.forRoot(),
        BrowserAnimationsModule
      ],
      providers: [
        {
          provide: STEPPER_GLOBAL_OPTIONS,
          useValue: {showError: true }
        },
        NgxImageCompressService
      ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
