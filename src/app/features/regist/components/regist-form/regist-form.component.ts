import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatHorizontalStepper, MatVerticalStepper} from '@angular/material';
import {MediaMatcher} from '@angular/cdk/layout';
import {MobileQueryUtil} from '../../../../shared/utils/mobile-query.util';
import {PlaceFormComponent} from '../place-form/place-form.component';
import {MealFormComponent} from '../meal-form/meal-form.component';
import {When} from '../../models/when.enum';
import {Router} from '@angular/router';
import {Marker, Place} from '../../../../shared/models/place.interface';
import {Meal} from '../../../../shared/models/meal.interface';
import {Subscription} from 'rxjs';
import {MealService} from '../../../../shared/services/object-handler/meal/meal.service';
import {PlaceService} from '../../../../shared/services/object-handler/place/place.service';
import {PersonalDataService} from '../../../../shared/services/object-handler/app-user/personal-data.service';
import {NetworkService} from '../../../../shared/services/network/network.service';

@Component({
  selector: 'app-regist-form',
  templateUrl: './regist-form.component.html',
  styleUrls: ['./regist-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegistFormComponent implements OnInit, AfterContentInit, AfterViewInit, OnDestroy {

  @ViewChild(PlaceFormComponent, {static: false}) firstFormGroup: PlaceFormComponent;
  @ViewChild(MealFormComponent, {static: false}) secondFormGroup: MealFormComponent;

  private earlier = false;
  private selectedMarker: Marker = null;
  private mobileQueryUtil: MobileQueryUtil;
  private subsc: Subscription[] = [];
  private currentAllPlaces: Place[] = [];
  placeFormGroup: FormGroup;
  mealFormGroup: FormGroup;
  isOnline: boolean;

  constructor(private formBuilder: FormBuilder, private router: Router,
              private mealService: MealService, private placeService: PlaceService,
              private ref: ChangeDetectorRef, private personalDataService: PersonalDataService,
              private networkService: NetworkService,
              changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.isOnline = this.networkService.isOnline;
    this.subsc.push(this.networkService.connectionChanged.subscribe(value => this.isOnline = value));
    this.mobileQueryUtil = new MobileQueryUtil(changeDetectorRef, media);
    this.placeService.getAll();
    this.subsc.push(this.placeService.allPlace.subscribe(places => {
      this.currentAllPlaces = places ? places : [];
    }));
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.placeFormGroup = this.firstFormGroup.placeFormGroup;
      this.mealFormGroup = this.secondFormGroup.mealFormGroup;
      this.ref.markForCheck();
    }, 0);
  }

  ngAfterContentInit() {
    this.mobileQueryUtil.start();
  }

  ngOnDestroy() {
    this.subsc.forEach(x => x.unsubscribe());
  }

  reset(stepperId: MatHorizontalStepper | MatVerticalStepper): void {
    stepperId.reset();
    this.firstFormGroup.placeFormGroup.patchValue({
      when: When.Now
    });
  }

  private calcAvgRate(newMealRate: number, mealKeysLength: number, actualAvgValue: number, addingOrRemoving: 1 | -1 = 1): number {
    return ((actualAvgValue * mealKeysLength) + (newMealRate * addingOrRemoving)) / (mealKeysLength + addingOrRemoving);
  }

  private addMealAndPlace(meal: Meal, place: Place, notExist: boolean) {
    meal.$key = this.mealService.post(meal) as string;
    place.mealKeys.push(meal.$key);
    if (notExist) {
      place.$key = this.placeService.post(place) as string;
      this.personalDataService.putPlaceKey(place.$key);
    } else {
      this.placeService.put(place);
    }
  }

  confirm(): void {
    const meal: Meal = {
      $key: undefined,
      name: this.mealFormGroup.controls.name.value,
      date: this.mealFormGroup.controls.date.value.getTime(),
      rate: this.mealFormGroup.controls.rate.value,
      picture: this.mealFormGroup.controls.picture.value
    };
    const filter = this.placeFormGroup.controls.name.value;
    let resultPlace = null;
    const result = this.currentAllPlaces.filter(x => x.name.toLocaleLowerCase().trim()
      .includes(filter.toLocaleLowerCase().trim()));
    if (result !== null && result.length !== 0) {
      resultPlace = result[0];
    }
    const avgRate = resultPlace ? this.calcAvgRate(meal.rate, resultPlace.mealKeys.length, resultPlace.avgRate) :
      this.calcAvgRate(meal.rate, 0, 0);
    const newPlace: Place = {
      $key: resultPlace ? resultPlace.$key : undefined,
      name: resultPlace ? resultPlace.name : this.placeFormGroup.controls.name.value,
      marker: resultPlace ? resultPlace.marker : this.selectedMarker,
      mealKeys: resultPlace ? resultPlace.mealKeys : [],
      avgRate
    };
    this.addMealAndPlace(meal, newPlace, resultPlace === null);
    this.router.navigate(['']);
  }

  setWhen(event: boolean) {
    this.earlier = event;
  }

  setSelectedMarker(event: Marker) {
    this.selectedMarker = event;
  }

  get isEarlierDate(): boolean {
    return this.earlier;
  }

  get mobileQuery() {
    return this.mobileQueryUtil.mobileQuery;
  }

  // get placeFormGroup(): FormGroup {
  //   return this.firstFormGroup ? this.firstFormGroup.placeFormGroup : null;
  // }
  //
  // get mealFormGroup(): FormGroup {
  //   return this.firstFormGroup ? this.secondFormGroup.mealFormGroup : null;
  // }
}
