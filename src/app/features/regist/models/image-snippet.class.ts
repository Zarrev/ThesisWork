export class ImageSnippet {

  constructor(private _src: string, public file: File) {
  }

  get src() {
    if (this._src == null) {
      return '/assets/img/add_img.svg';
    }
    return this._src;
  }
}
