import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistPageComponent } from './regist-page.component';
import {RegistFormComponent} from '../../components/regist-form/regist-form.component';
import {CdkStepperModule, STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {RouterTestingModule} from '@angular/router/testing';
import {MatStepperModule} from '@angular/material';
import {SharedModule} from '../../../../shared/shared.module';
import {RatingModule} from 'ngx-bootstrap';
import {PlaceFormComponent} from '../../components/place-form/place-form.component';
import {MealFormComponent} from '../../components/meal-form/meal-form.component';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxImageCompressService} from 'ngx-image-compress';

describe('RegistPageComponent', () => {
  let component: RegistPageComponent;
  let fixture: ComponentFixture<RegistPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistPageComponent, RegistFormComponent, PlaceFormComponent, MealFormComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        CdkStepperModule,
        RouterTestingModule,
        MatStepperModule,
        SharedModule.forRoot(),
        RatingModule.forRoot(),
        BrowserAnimationsModule
      ],
      providers: [
        {
          provide: STEPPER_GLOBAL_OPTIONS,
          useValue: {showError: true }
        },
        NgxImageCompressService
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
