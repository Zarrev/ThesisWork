import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistPageComponent } from './pages/regist-page/regist-page.component';
import { RegistFormComponent } from './components/regist-form/regist-form.component';
import { RegistRoutingModule } from './regist-routing.module';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {SharedModule} from '../../shared/shared.module';
import {MapComponent} from '../../shared/components/map/map.component';
import { MealFormComponent } from './components/meal-form/meal-form.component';
import { PlaceFormComponent } from './components/place-form/place-form.component';
import {RatingModule} from 'ngx-bootstrap/rating';
import {NgxImageCompressService} from 'ngx-image-compress';

@NgModule({
  declarations: [RegistPageComponent, RegistFormComponent, MealFormComponent, PlaceFormComponent],
  imports: [
    CommonModule,
    RegistRoutingModule,
    SharedModule.forRoot(),
    RatingModule.forRoot()
  ],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: {showError: true }
    },
    NgxImageCompressService
  ],
  entryComponents: [
    MapComponent
  ]
})
export class RegistModule { }
