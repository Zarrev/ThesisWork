import {async, TestBed} from '@angular/core/testing';

import { NearNotifierService } from './near-notifier.service';
import {SharedModule} from '../../../../shared/shared.module';
import {ServiceWorkerModule} from '@angular/service-worker';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('NearNotifierService', () => {
  beforeEach(async(() => TestBed.configureTestingModule({
    imports: [
      AngularFireModule.initializeApp(environment.firebaseConfig),
      AngularFireDatabaseModule,
      SharedModule.forRoot(),
      BrowserAnimationsModule,
      ServiceWorkerModule.register('', {enabled: false}),
      RouterTestingModule
    ],
    providers: [
      NearNotifierService
    ]
  })));

  it('should be created', () => {
    const service: NearNotifierService = TestBed.get(NearNotifierService);
    expect(service).toBeTruthy();
  });
});
