import {Injectable, OnDestroy} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {openSnackBar} from '../../../../shared/utils/snackbar';
import {PlaceService} from '../../../../shared/services/object-handler/place/place.service';
import {Place} from '../../../../shared/models/place.interface';
import {Observable, of, Subscription} from 'rxjs';
import {HaversineService} from 'ng2-haversine';
import {LocationService} from '../../../../shared/services/location/location.service';
import {catchError, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {SwPush} from '@angular/service-worker';

@Injectable()
export class NearNotifierService implements OnDestroy {

  private notificationOptions: NotificationOptions = {
    body: 'Explore this place again!',
    image: '../../../../assets/img/location-pin.svg',
    vibrate: [200, 100, 200, 100, 200, 100, 200],
    icon: '../../../../assets/icons/icon-72x72.png',
    actions: [
      {
        action: 'explore',
        title: 'Explore'
      }
    ],
    tag: 'woym-explore',
    renotify: false,
    silent: false
  };
  private places: Place[] = [];
  private place: Place = null;
  private subs: Subscription[] = [];

  constructor(private snackbar: MatSnackBar, private placeService: PlaceService,
              private haversineService: HaversineService, private locationService: LocationService,
              private router: Router, private swPush: SwPush) {
    this.subs.push(this.placeService.allPlace.subscribe(places => {
      if (!places) {
        return;
      }
      this.places = places;
    }));
    this.placeService.getAll();
    this.subs.push(this.swPush.notificationClicks.subscribe(notification => {
      if (notification.action === 'explore') {
        this.jump(this.place);
      }
    }));
  }

  sendNotification() {
    if (!this.checkNotificationCapabilities()) {
      return;
    }

    Notification.requestPermission().then(value => {
      this.subs.push(this.locationService.getMyPosition().subscribe(position => {}, error =>  openSnackBar(this.snackbar, error)));
      if (value === 'granted') {
        try {
          navigator.serviceWorker.getRegistration()
            .then(reg => {
              const subscription = this.checkPlace().subscribe(place => {
                this.place = place;
                if (!place) {
                  this.notificationOptions.body = 'No available place for you this time :(';
                  reg.showNotification('Hi there - here you can find the closest place!', this.notificationOptions);
                  return;
                }
                reg.showNotification('Hi there - here you can find the closest place!', this.notificationOptions);
              }, error => {
                openSnackBar(this.snackbar, 'Error: ' + error);
                },
                () => {
                  subscription.unsubscribe();
              });
            })
            .catch(err => openSnackBar(this.snackbar, 'Service Worker registration error: ' + err));
        } catch (err) {
          openSnackBar(this.snackbar, 'Notification API error: ' + err);
        }
      } else {
        openSnackBar(this.snackbar, 'You have not allowed the notifications!');
      }
    });
  }

  private checkNotificationCapabilities(): boolean {
    if (!('Notification' in window) || !('ServiceWorkerRegistration' in window)) {
      openSnackBar(this.snackbar, 'Notifications are not supported!');
      return false;
    }

    return true;
  }

  private checkPlace(): Observable<Place> {
    return this.locationService.myLocation.pipe(map( myLocation => {
      let closerPlace: Place = null;
      let distance = 40075;
      this.places.forEach(place => {
        const realDistance: number = this.haversineService.getDistanceInKilometers(place.marker, myLocation);
        if (realDistance < distance) {
          distance = realDistance;
          closerPlace = place;
        }
      });
      if (distance === 40075) {
        return null;
      }
      return closerPlace;
    }), catchError(error => {
      return of(error);
    }));
  }

  private jump(place: Place) {
    this.router.navigate(['/history/map', {longitude: place.marker.longitude, latitude: place.marker.latitude}]);
  }

  ngOnDestroy(): void {
    this.subs.forEach(x => x.unsubscribe());
  }
}

export interface SWInterface extends ServiceWorkerContainerEventMap {
  asd: string;
}
