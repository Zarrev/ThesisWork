import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HungryRoutingModule } from './hungry-routing.module';
import {NearNotifierService} from './services/push-noti/near-notifier.service';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HungryRoutingModule,
    SharedModule.forRoot()
  ],
  providers: [
    NearNotifierService
  ]
})
export class HungryModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: HungryModule,
      providers: [NearNotifierService]
    };
  }
}
