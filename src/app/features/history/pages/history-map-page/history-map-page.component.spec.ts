import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryMapPageComponent } from './history-map-page.component';
import {SharedModule} from '../../../../shared/shared.module';
import {MarkerMapComponent} from '../../components/marker-map/marker-map.component';
import {RouterTestingModule} from '@angular/router/testing';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('HistoryMapPageComponent', () => {
  let component: HistoryMapPageComponent;
  let fixture: ComponentFixture<HistoryMapPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryMapPageComponent, MarkerMapComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        SharedModule,
        BrowserAnimationsModule,
        RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryMapPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
