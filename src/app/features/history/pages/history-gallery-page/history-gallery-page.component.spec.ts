import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryGalleryPageComponent } from './history-gallery-page.component';
import {RouterModule} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {SharedModule} from '../../../../shared/shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('HistoryGalleryPageComponent', () => {
  let component: HistoryGalleryPageComponent;
  let fixture: ComponentFixture<HistoryGalleryPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryGalleryPageComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        RouterTestingModule,
        RouterModule,
        BrowserAnimationsModule,
        SharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    const dummyElement = document.createElement('div');
    dummyElement.setAttribute('id', 'mat-sidenav-content');
    document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
    fixture = TestBed.createComponent(HistoryGalleryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
