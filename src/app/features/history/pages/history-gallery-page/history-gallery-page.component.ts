import {Component, OnDestroy, OnInit} from '@angular/core';
@Component({
  selector: 'app-history-gallery-page',
  templateUrl: './history-gallery-page.component.html',
  styleUrls: ['./history-gallery-page.component.scss']
})
export class HistoryGalleryPageComponent implements OnInit, OnDestroy {

  private backgroundContainer: HTMLElement;

  constructor() { }

  ngOnInit() {
    this.backgroundContainer = document.getElementById('mat-sidenav-content');
    this.backgroundContainer.classList.add('div__background-image--picture');
  }

  ngOnDestroy(): void {
    this.backgroundContainer.classList.remove('div__background-image--picture');
  }
}
