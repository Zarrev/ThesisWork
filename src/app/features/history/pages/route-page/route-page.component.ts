import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NetworkService} from '../../../../shared/services/network/network.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-route-page',
  templateUrl: './route-page.component.html',
  styleUrls: ['./route-page.component.scss']
})
export class RoutePageComponent implements OnInit, OnDestroy {

  private readonly routeMap = 'map';
  private route = this.routeMap;
  private listIcon = 'list_alt';
  private mapIcon = 'near_me';
  private subsc: Subscription[] = [];

  isOnline: boolean;
  icon = 'near_me';

  constructor(private router: Router, private networkService: NetworkService) {
    this.isOnline = this.networkService.isOnline;
    this.subsc.push(this.networkService.connectionChanged.subscribe(value => this.isOnline = value));
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.subsc.forEach(x => x.unsubscribe());
  }

  onClick(): void {
    let navigateUrl = this.router.url.split('/');
    let index = navigateUrl.indexOf(this.routeMap);
    if (index !== -1) {
      this.route = 'gallery';
      navigateUrl = navigateUrl.slice(0, index);
      this.icon = this.mapIcon;
    } else {
      this.route = this.routeMap;
      index = navigateUrl.indexOf('gallery');
      navigateUrl = navigateUrl.slice(0, index);
      this.icon = this.listIcon;
    }
    this.route = '/' + navigateUrl.join('/') + '/' + this.route;
    this.router.navigate([this.route]);
  }
}
