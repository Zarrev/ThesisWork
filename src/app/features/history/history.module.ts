import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HistoryRoutingModule} from './history-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {HistoryMapPageComponent} from './pages/history-map-page/history-map-page.component';
import {HistoryGalleryPageComponent} from './pages/history-gallery-page/history-gallery-page.component';
import {RoutePageComponent} from './pages/route-page/route-page.component';
import {PlaceTableComponent} from './components/place-table/place-table.component';
import {MealGalleryComponent} from './components/meal-gallery/meal-gallery.component';
import {CarouselModule} from 'ngx-bootstrap/carousel';
import {MarkerMapComponent} from './components/marker-map/marker-map.component';
import {RatingModule} from 'ngx-bootstrap/rating';
import {ModalModule} from 'ngx-bootstrap/modal';
import { AppCarouselSwipeDirective } from './directives/app-carousel-swipe.directive';
import { ShareComponent } from './components/share/share.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    HistoryMapPageComponent,
    HistoryGalleryPageComponent,
    RoutePageComponent,
    PlaceTableComponent,
    MealGalleryComponent,
    MarkerMapComponent,
    AppCarouselSwipeDirective,
    ShareComponent
  ],
  imports: [
    CommonModule,
    HistoryRoutingModule,
    CarouselModule,
    RatingModule.forRoot(),
    ModalModule.forRoot(),
    SharedModule.forRoot(),
    MatProgressSpinnerModule
  ]
})
export class HistoryModule {
}
