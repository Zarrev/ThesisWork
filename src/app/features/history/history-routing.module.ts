import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HistoryMapPageComponent} from './pages/history-map-page/history-map-page.component';
import {RoutePageComponent} from './pages/route-page/route-page.component';
import {HistoryGalleryPageComponent} from './pages/history-gallery-page/history-gallery-page.component';
import {PlaceTableComponent} from './components/place-table/place-table.component';
import {MealGalleryComponent} from './components/meal-gallery/meal-gallery.component';

const routes: Routes = [
  {
    path: '',
    component: RoutePageComponent,
    children: [
      {
        path: '',
        redirectTo: 'gallery',
        pathMatch: 'full'
      },
      {
        path: 'map',
        component: HistoryMapPageComponent
      },
      {
        path: 'gallery',
        component: HistoryGalleryPageComponent,
        children: [
          {
            path: '',
            redirectTo: 'places',
            pathMatch: 'full'
          },
          {
            path: 'places',
            component: PlaceTableComponent
          },
          {
            path: 'meals/:placeId/:mealId',
            component: MealGalleryComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoryRoutingModule { }
