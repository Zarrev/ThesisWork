import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Message, Payload} from '../../../../shared/models/community-models/message.interface';
import {MessagesService} from '../../../../shared/services/database/firebase/community/messages.service';
import {PersonalDataService} from '../../../../shared/services/object-handler/app-user/personal-data.service';
import {AppUser} from '../../../../shared/models/app-user.interface';
import {FormControl, Validators} from '@angular/forms';
import {UsersService} from '../../../../shared/services/database/firebase/community/users.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.scss']
})
export class ShareComponent implements OnInit, OnDestroy {

  @Input() payload: Payload;
  @Output() isSuccessShare = new EventEmitter<boolean>();
  email = new FormControl('', [Validators.required, Validators.email]);
  loading = false;
  private subs: Subscription[] = [];
  private user: AppUser;

  constructor(private msgService: MessagesService, private personalDataService: PersonalDataService, private usersService: UsersService) {
    personalDataService.user.subscribe(user => {
      if (!user.fireUser) {
        return;
      }
      this.user = user;
    });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.subs.forEach(x => x.unsubscribe());
  }

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
      this.email.hasError('email') ? 'Not a valid email' :
        '';
  }

  share(text?: string) {
    this.loading = true;
    if (text) {
      this.payload.text = text;
    }
    this.subs.push(this.usersService.getReceiverUser(this.email.value).subscribe(users => {
      console.log(users);
      this.msgService.sendMyMessage(users[0].key, this.createMessage()).then(value => {
        this.loading = false;
        this.isSuccessShare.emit(true);
      }).catch(error => {
        this.loading = false;
        this.isSuccessShare.emit(false);
      });
    }));
  }

  private createMessage(): Message {
    let name = this.user.fireUser.displayName;
    let picture = this.user.fireUser.photoURL;
    if (this.user.profile) {
      if (this.user.profile.nickname) {
        name = this.user.profile.nickname;
      }
      if (this.user.profile.profilePicture) {
        picture = this.user.profile.profilePicture;
      }
    }
    return {
      payload: this.payload, timestamp: new Date().getTime(),
      email: this.user.fireUser.email, name, picture, senderId: this.user.fireUser.uid
    };
  }
}
