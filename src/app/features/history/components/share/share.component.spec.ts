import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareComponent } from './share.component';
import {ModalModule} from 'ngx-bootstrap';
import {SharedModule} from '../../../../shared/shared.module';
import {MatProgressSpinnerModule} from '@angular/material';
import {MessagesService} from '../../../../shared/services/database/firebase/community/messages.service';
import {PersonalDataService} from '../../../../shared/services/object-handler/app-user/personal-data.service';
import {UsersService} from '../../../../shared/services/database/firebase/community/users.service';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('ShareComponent', () => {
  let component: ShareComponent;
  let fixture: ComponentFixture<ShareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        ModalModule.forRoot(),
        SharedModule.forRoot(),
        MatProgressSpinnerModule,
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      providers: [
        MessagesService,
        PersonalDataService,
        UsersService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
