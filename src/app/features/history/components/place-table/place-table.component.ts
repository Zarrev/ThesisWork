import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';
import {Place} from '../../../../shared/models/place.interface';
import {PlaceService} from '../../../../shared/services/object-handler/place/place.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-place-table',
  templateUrl: './place-table.component.html',
  styleUrls: ['./place-table.component.scss']
})
export class PlaceTableComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  private subsc: Subscription[] = [];

  displayedColumns: string[] = ['name', 'meals_number', 'average_rate', 'button'];
  dataSource: MatTableDataSource<Place>;

  constructor(private router: Router, private placeService: PlaceService) {
    this.placeService.getAll();
    this.dataSource = new MatTableDataSource([]);
    this.subsc.push(this.placeService.allPlace.subscribe(places => {
      if (!places || places.length === 0) {
        return;
      }
      this.dataSource.data = places;
    }));
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  navigate(place: Place) {
    this.placeService.setCurrentPlace(place);
    const firstMealKey = this.dataSource.data.filter(x => x.$key === place.$key)[0].mealKeys[0];
    const baseNavUrl = this.router.url.split('/');
    baseNavUrl.pop();
    baseNavUrl.push('meals');
    this.router.navigate(['/' + baseNavUrl.join('/'), place.$key, firstMealKey]);
  }

  ngOnDestroy(): void {
    this.subsc.forEach(x => x.unsubscribe());
  }
}
