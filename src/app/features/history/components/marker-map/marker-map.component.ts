import {Component, OnDestroy, OnInit} from '@angular/core';
import {Place} from '../../../../shared/models/place.interface';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {PlaceService} from '../../../../shared/services/object-handler/place/place.service';

@Component({
  selector: 'app-marker-map',
  templateUrl: './marker-map.component.html',
  styleUrls: ['./marker-map.component.scss']
})
export class MarkerMapComponent implements OnInit, OnDestroy {

  private subsc: Subscription[] = [];
  places: Place[] = [];
  routerLinkFunction: (place: Place) => string[];

  constructor(private router: Router, private placeService: PlaceService) {
    this.placeService.getAll();
    this.subsc.push(this.placeService.allPlace.subscribe(places => {
      if (!places || places.length === 0) {
        return;
      }
      this.places = places;
    }));
    this.routerLinkFunction = (place: Place) => {

      const baseNavUrl = this.router.url.split('/');
      baseNavUrl.pop();
      baseNavUrl.push('gallery/meals');
      return ['/' + baseNavUrl.join('/'), place.$key, place.mealKeys[0]];
    };
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subsc.forEach(x => x.unsubscribe());
  }
}
