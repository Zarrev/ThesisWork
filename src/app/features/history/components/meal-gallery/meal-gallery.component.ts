import {AfterContentChecked, AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {Meal} from '../../../../shared/models/meal.interface';
import {DomSanitizer} from '@angular/platform-browser';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Place} from '../../../../shared/models/place.interface';
import {ActivatedRoute, Router} from '@angular/router';
import {PlatformLocation} from '@angular/common';
import {MediaMatcher} from '@angular/cdk/layout';
import {MobileQueryUtil} from '../../../../shared/utils/mobile-query.util';
import {MealService} from '../../../../shared/services/object-handler/meal/meal.service';
import {PlaceService} from '../../../../shared/services/object-handler/place/place.service';
import {Subscription} from 'rxjs';
import {NetworkService} from '../../../../shared/services/network/network.service';
import {PersonalDataService} from '../../../../shared/services/object-handler/app-user/personal-data.service';
import {Payload} from '../../../../shared/models/community-models/message.interface';

@Component({
  selector: 'app-meal-gallery',
  templateUrl: './meal-gallery.component.html',
  styleUrls: ['./meal-gallery.component.scss']
})
export class MealGalleryComponent implements OnInit, OnDestroy, AfterViewInit, AfterContentChecked {


  private _modalRef: BsModalRef;
  private _selectedMeal: Meal;
  private firstActive = this.route.snapshot.paramMap.get('mealId');
  private currentPlaceKey = this.route.snapshot.paramMap.get('placeId');
  private _meals: Meal[] = [];
  private mobileQueryUtil: MobileQueryUtil;
  private subsc: Subscription[] = [];
  private currentPlace: Place;

  isOnline: boolean;
  sharing = false;
  payload: Payload;
  activeSlideIndex = 0;

  constructor(private modalService: BsModalService, private sanitizer: DomSanitizer,
              private router: Router, private route: ActivatedRoute,
              private mealService: MealService, private placeService: PlaceService,
              private location: PlatformLocation, private ref: ChangeDetectorRef,
              private personalDataService: PersonalDataService, private networkService: NetworkService,
              changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.isOnline = this.networkService.isOnline;
    this.subsc.push(this.networkService.connectionChanged.subscribe(value => this.isOnline = value));
    this.mealService.getAll();
    this.placeService.get(this.currentPlaceKey);
    this.location.onPopState(() => {
      if (this._modalRef !== undefined && this._modalRef !== null) {
        this._modalRef.hide();
      }
    });
    this.subsc.push(this.mealService.allMeal.subscribe(meals => {
      if (!meals) {
        return;
      }
      this.placeService.get(this.currentPlaceKey);
      this.getMealsForPlace(meals);
    }));
    this.mobileQueryUtil = new MobileQueryUtil(changeDetectorRef, media);
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.setActive();
      this.ref.markForCheck();
    }, 0);
  }

  ngAfterContentChecked() {
    this.mobileQueryUtil.start();
  }

  ngOnDestroy() {
    this.mobileQueryUtil.removeEvent();
    this.subsc.forEach(x => x.unsubscribe());
  }

  private getMealsForPlace(meals: Meal[]): void {
    this.subsc.push(this.placeService.currentPlace.subscribe(place => {
      this.currentPlace = place;
      this._meals = meals.filter(x => place.mealKeys.includes(x.$key));
    }));
  }

  openModal(template: TemplateRef<any>, img: Meal) {
    this._selectedMeal = img;
    this._modalRef = this.modalService.show(template, Object.assign({}, {class: 'modal-lg'}));
  }

  setActive() {
    for (let i = 0; i < this.meals.length; i++) {
      if (this.meals[i].$key === this.firstActive) {
        this.activeSlideIndex = i;
        return;
      }
    }
    this.activeSlideIndex = 0;
  }

  onChange(event: number) {
    const baseNavUrl = this.router.url.split('/');
    baseNavUrl.pop();
    baseNavUrl.pop();
    this.router.navigate(['/' + baseNavUrl.join('/'), this.currentPlaceKey, this._meals[event].$key]);
  }

  jump(place: Place) {
    this.modalRef.hide();
    let baseNavLink = this.router.url.split('/');
    baseNavLink = baseNavLink.slice(0, baseNavLink.indexOf('gallery'));
    baseNavLink.push('map');
    this.router.navigate(['/' + baseNavLink.join('/'), {longitude: place.marker.longitude, latitude: place.marker.latitude}]);
  }

  remove(meal: Meal) {
    this.modalRef.hide();
    const mealRate = meal.rate;
    this.mealService.delete(meal);
    this.currentPlace.avgRate = this.calcAvgRate(mealRate, this.currentPlace.mealKeys.length, this.currentPlace.avgRate, -1);
    this.currentPlace.mealKeys = this.currentPlace.mealKeys.filter(mKey => mKey !== meal.$key);
    if (this.currentPlace.mealKeys.length === 0) {
      this.placeService.delete(this.currentPlace);
      this.personalDataService.delPlaceKey(this.currentPlace.$key);
    } else {
      this.placeService.put(this.currentPlace);
    }
    let baseNavLink = this.router.url.split('/');
    baseNavLink = baseNavLink.slice(0, baseNavLink.indexOf('meals'));
    baseNavLink.push('places');
    this.router.navigate(['/' + baseNavLink.join('/')]);
  }

  private calcAvgRate(newMealRate: number, mealKeysLength: number, actualAvgValue: number, addingOrRemoving: 1 | -1 = 1): number {
    return ((actualAvgValue * mealKeysLength) + (newMealRate * addingOrRemoving)) / (mealKeysLength + addingOrRemoving);
  }

  share(place: Place, meal: Meal) {
    this.payload = {
      avgRate: place.avgRate,
      placeName: place.name,
      marker: place.marker,
      meal: {
        rate: meal.rate,
        name: meal.name,
        picture: meal.picture
      }
    };
    this.sharing = true;
  }

  place(): Place {
    return this.currentPlace;
  }

  get modalRef(): BsModalRef {
    return this._modalRef;
  }

  get meals(): Meal[] {
    if (this._meals !== undefined && this._meals !== null) {
      return this._meals;
    } else {
      return [];
    }
  }

  get selectedMeal(): Meal {
    return this._selectedMeal;
  }

  get mobileQuery() {
    return this.mobileQueryUtil.mobileQuery;
  }
}
