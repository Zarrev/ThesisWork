import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealGalleryComponent } from './meal-gallery.component';
import {SharedModule} from '../../../../shared/shared.module';
import {CarouselModule, ModalModule, RatingModule} from 'ngx-bootstrap';
import {RouterTestingModule} from '@angular/router/testing';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ShareComponent} from '../share/share.component';
import {MatProgressSpinnerModule} from '@angular/material';

describe('MealGalleryComponent', () => {
  let component: MealGalleryComponent;
  let fixture: ComponentFixture<MealGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealGalleryComponent, ShareComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        RouterTestingModule,
        CarouselModule,
        RatingModule.forRoot(),
        ModalModule.forRoot(),
        SharedModule.forRoot(),
        BrowserAnimationsModule,
        MatProgressSpinnerModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
