import { AppCarouselSwipeDirective } from './app-carousel-swipe.directive';
import {CarouselModule} from 'ngx-bootstrap';
import {TestBed} from '@angular/core/testing';

describe('AppCarouselSwipeDirective', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      CarouselModule
    ]
  }));
  it('should create an instance', () => {
    const directive = new AppCarouselSwipeDirective(null, null, null);
    expect(directive).toBeTruthy();
  });
});
