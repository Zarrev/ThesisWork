import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilePageComponent } from './profile-page.component';
import {ProfileComponent} from '../components/profile/profile.component';
import {MatCardModule} from '@angular/material';
import {TooltipModule} from 'ngx-bootstrap';
import {SharedModule} from '../../../shared/shared.module';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxImageCompressService} from 'ngx-image-compress';

describe('ProfilePageComponent', () => {
  let component: ProfilePageComponent;
  let fixture: ComponentFixture<ProfilePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilePageComponent, ProfileComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        SharedModule,
        MatCardModule,
        BrowserAnimationsModule,
        TooltipModule.forRoot(),
        RouterTestingModule
      ],
      providers: [
        NgxImageCompressService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    const dummyElement = document.createElement('div');
    dummyElement.setAttribute('id', 'mat-sidenav-content');
    document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
    fixture = TestBed.createComponent(ProfilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
