import {Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit, OnDestroy {

  private backgroundContainer: HTMLElement;

  constructor() { }

  ngOnInit() {
    this.backgroundContainer = document.getElementById('mat-sidenav-content');
    this.backgroundContainer.classList.add('div__background-image--picture');
  }

  ngOnDestroy(): void {
    this.backgroundContainer.classList.remove('div__background-image--picture');
  }
}
