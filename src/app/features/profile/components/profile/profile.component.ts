/* tslint:disable:no-bitwise */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppUser} from '../../../../shared/models/app-user.interface';
import {Place} from '../../../../shared/models/place.interface';
import {ImageSnippet} from '../../../regist/models/image-snippet.class';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PersonalDataService} from '../../../../shared/services/object-handler/app-user/personal-data.service';
import {Subscription} from 'rxjs';
import {PlaceService} from '../../../../shared/services/object-handler/place/place.service';
import {NgxImageCompressService} from 'ngx-image-compress';
import {NetworkService} from '../../../../shared/services/network/network.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

  user: AppUser;
  data: {
    labels: string[],
    datasets: {
      data: number[],
      backgroundColor: string[],
      hoverBackgroundColor: string[]
    }[]
  } = {
    labels: [],
    datasets: [{
      data: [],
      backgroundColor: [],
      hoverBackgroundColor: []
    }]
  };
  options: any;
  editModeOn = false;
  selectedFile = new ImageSnippet(null, null);
  places: Place[] = [];
  isOnline: boolean;
  private subsc: Subscription[] = [];
  private _formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder, private personalDataService: PersonalDataService, private networkService: NetworkService,
              private placeService: PlaceService, private imageCompress: NgxImageCompressService) {
    this.isOnline = this.networkService.isOnline;
    this.subsc.push(this.networkService.connectionChanged.subscribe(value => this.isOnline = value));
    this.subsc.push(this.personalDataService.user.subscribe(user => {
      if (!user.fireUser) {
        return;
      }
      this.user = user;
      this.placeService.getAll();
      this.subsc.push(this.placeService.allPlace.subscribe(places => {
        if (!places || places.length === 0) {
          return;
        }
        this.places = places;
        this.places.forEach((x, i) => {
          this.data.labels.push(x.name);
          this.data.datasets[0].data.push(x.mealKeys.length);
          this.data.datasets[0].backgroundColor.push(this.rainbow(this.places.length, i));
          this.data.datasets[0].hoverBackgroundColor.push(this.rainbow(this.places.length, i));
        });
      }));
    }));
    this._formGroup = this.formBuilder.group({
      picture: [null],
      nickname: [this.user ? this.user.profile.nickname : ''],
      notify: [this.user ? this.user.settings.notifyNumber : 0, [Validators.min(0), Validators.max(5)]]
    });

    this.options = {
      legend: {
        display: false
      }
    };
  }

  ngOnInit() {
  }

  onClick(): void {
    if (this.editModeOn) {
      this.saveChanges();
    }
    this.editModeOn = !this.editModeOn;
  }

  processFile() {
    this.imageCompress.uploadFile().then(({image, orientation}) => {
      this.imageCompress.compressFile(image, orientation, 50, 50).then(
        result => {
          this._formGroup.patchValue({
            picture: result
          });
          this.selectedFile = new ImageSnippet(result, null);
        }
      );

    });
  }

  rainbow(numOfSteps, step) {
    // tslint:disable-next-line:one-variable-per-declaration
    let r, g, b;
    const h = step / numOfSteps;
    const i = ~~(h * 6);
    const f = h * 6 - i;
    const q = 1 - f;
    switch (i % 6) {
      case 0:
        r = 1; g = f; b = 0; break;
      case 1:
        r = q; g = 1; b = 0; break;
      case 2:
        r = 0; g = 1; b = f; break;
      case 3:
        r = 0; g = q; b = 1;
        break;
      case 4:
        r = f; g = 0; b = 1; break;
      case 5:
        r = 1; g = 0; b = q; break;
    }
    const c = '#' + ('00' + (~~(r * 255))
      .toString(16)).slice(-2) + ('00' + (~~(g * 255))
      .toString(16)).slice(-2) + ('00' + (~~(b * 255))
      .toString(16)).slice(-2);
    return (c);
  }

  private saveChanges(): void {
    const values: any[] = [
      this._formGroup.controls.notify.value,
      this._formGroup.controls.picture.value,
      this._formGroup.controls.nickname.value
    ];

    if (values[0] === this.user.settings.notifyNumber &&
      values[1] === this.user.profile.profilePicture &&
      values[2] === this.user.profile.nickname) {
      return;
    }

    if (values[0] !== null && values[0] !== undefined) {
      this.user.settings.notifyNumber = values[0];
    }

    if (values[1]) {
      this.user.profile.profilePicture = values[1];
    }

    if (values[2] || values[2] === '') {
      this.user.profile.nickname = values[2] === '' ? this.user.fireUser.displayName : values[2];
    }

    this.personalDataService.put(this.user);
  }

  get formGroup(): FormGroup {
    return this._formGroup;
  }

  ngOnDestroy(): void {
    this.subsc.forEach(x => x.unsubscribe());
  }
}
