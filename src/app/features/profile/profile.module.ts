import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { ProfileComponent } from './components/profile/profile.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { SharedModule } from '../../shared/shared.module';
import {NgxImageCompressService} from 'ngx-image-compress';


@NgModule({
  declarations: [
    ProfilePageComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
    TooltipModule.forRoot()
  ],
  providers: [
    NgxImageCompressService
  ]
})
export class ProfileModule { }
