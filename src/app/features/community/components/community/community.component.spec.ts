import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityComponent } from './community.component';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {SharedModule} from '../../../../shared/shared.module';
import {MatDialogModule, MatExpansionModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Message} from '../../../../shared/models/community-models/message.interface';

describe('CommunityComponent', () => {
  let component: CommunityComponent;
  let fixture: ComponentFixture<CommunityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        SharedModule.forRoot(),
        MatDialogModule,
        MatExpansionModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityComponent);
    component = fixture.componentInstance;
    component.message = {
      payload: {
        marker: {
          latitude: 0,
          longitude: 0
        },
        placeName: '',
        avgRate: 1,
        text: '',
        meal: {
          picture: '',
          name: '',
          rate: 1
        }
      },
      timestamp: new Date().getTime(),
      email: '',
      name: '',
      senderId: '',
      picture: ''
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
