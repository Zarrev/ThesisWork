import {Component, Input, OnInit} from '@angular/core';
import {Message} from '../../../../shared/models/community-models/message.interface';
import {MatDialog} from '@angular/material';
import {MapDialogComponent} from '../map-dialog/map-dialog.component';
import {Place} from '../../../../shared/models/place.interface';

@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.scss']
})
export class CommunityComponent implements OnInit {

  @Input() message: Message;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog(): void {
    const payload = this.message.payload;
    const place: Place = {marker: payload.marker, $key: '', mealKeys: [''], avgRate: payload.avgRate, name: payload.placeName};
    const dialogRef = this.dialog.open(MapDialogComponent, {
      maxHeight: '500px',
      maxWidth: '500px',
      data: place
    });
  }


}
