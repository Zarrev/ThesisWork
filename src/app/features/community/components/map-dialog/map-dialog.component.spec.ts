import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import { MapDialogComponent } from './map-dialog.component';
import {CommunityPageComponent} from '../../pages/community-page/community-page.component';
import {CommunityComponent} from '../community/community.component';
import {MapComponent} from '../../../../shared/components/map/map.component';
import {SharedModule} from '../../../../shared/shared.module';
import {MatDialog, MatDialogModule, MatDialogRef, MatExpansionModule} from '@angular/material';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {BrowserDynamicTestingModule} from '@angular/platform-browser-dynamic/testing';
import {OverlayContainer} from '@angular/cdk/overlay';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';

describe('MapDialogComponent', () => {
  let dialog: MatDialog;
  let overlayContainer: OverlayContainer;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapDialogComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        SharedModule.forRoot(),
        MatDialogModule,
        MatExpansionModule,
        BrowserAnimationsModule,
        RouterTestingModule
      ]
    });

    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [MapDialogComponent]
      }
    });

    TestBed.compileComponents();
  }));

  beforeEach(inject([MatDialog, OverlayContainer],
    (d: MatDialog, oc: OverlayContainer) => {
      dialog = d;
      overlayContainer = oc;
    })
  );

  afterEach(() => {
    overlayContainer.ngOnDestroy();
  });

  it('should open a dialog with a component', () => {
    const dialogRef = dialog.open(MapDialogComponent, {
      data: { param: '1' }
    });
    expect(dialogRef.componentInstance instanceof MapDialogComponent).toBe(true);
  });
});
