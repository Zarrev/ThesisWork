import {Component, Inject, Input, OnInit} from '@angular/core';
import {Marker, Place} from '../../../../shared/models/place.interface';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-map-dialog',
  templateUrl: './map-dialog.component.html',
  styleUrls: ['./map-dialog.component.scss']
})
export class MapDialogComponent implements OnInit {

  routerLinkFunction = (place: Place) => {
    return false;
  };

  constructor(public dialogRef: MatDialogRef<MapDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Place) { }

  ngOnInit() {
  }

  onClick(): void {
    this.dialogRef.close();
  }
}
