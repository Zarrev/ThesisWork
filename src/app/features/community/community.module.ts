import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommunityRoutingModule } from './community-routing.module';
import { CommunityPageComponent } from './pages/community-page/community-page.component';
import { CommunityComponent } from './components/community/community.component';
import {SharedModule} from '../../shared/shared.module';
import {MapDialogComponent} from './components/map-dialog/map-dialog.component';
import {MapComponent} from '../../shared/components/map/map.component';
import { MatDialogModule } from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';


@NgModule({
  declarations: [CommunityPageComponent, CommunityComponent, MapDialogComponent],
  imports: [
    CommonModule,
    SharedModule.forRoot(),
    MatDialogModule,
    MatExpansionModule,
    CommunityRoutingModule
  ],
  entryComponents: [
    MapDialogComponent,
    MapComponent
  ]
})
export class CommunityModule { }
