import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityPageComponent } from './community-page.component';
import {CommunityComponent} from '../../components/community/community.component';
import {SharedModule} from '../../../../shared/shared.module';
import {MatDialogModule, MatExpansionModule} from '@angular/material';
import {MapDialogComponent} from '../../components/map-dialog/map-dialog.component';
import {MapComponent} from '../../../../shared/components/map/map.component';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../../../../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {RouterTestingModule} from '@angular/router/testing';

describe('CommunityPageComponent', () => {
  let component: CommunityPageComponent;
  let fixture: ComponentFixture<CommunityPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityPageComponent, CommunityComponent, MapDialogComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        SharedModule.forRoot(),
        MatDialogModule,
        MatExpansionModule,
        RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    const dummyElement = document.createElement('div');
    dummyElement.setAttribute('id', 'mat-sidenav-content');
    document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
    fixture = TestBed.createComponent(CommunityPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
