import {Component, OnDestroy, OnInit} from '@angular/core';
import {MessagesService} from '../../../../shared/services/database/firebase/community/messages.service';
import {Observable, Subscription} from 'rxjs';
import {Message} from '../../../../shared/models/community-models/message.interface';
import {PersonalDataService} from '../../../../shared/services/object-handler/app-user/personal-data.service';

@Component({
  selector: 'app-community-page',
  templateUrl: './community-page.component.html',
  styleUrls: ['./community-page.component.scss']
})
export class CommunityPageComponent implements OnInit, OnDestroy {

  private backgroundContainer: HTMLElement;
  messages$: Observable<Message[]> = new  Observable<Message[]>();
  private myKey: string;
  private xMult = 1;
  private subs: Subscription[] = [];

  constructor(private msgService: MessagesService, private personalDataService: PersonalDataService) {
    this.subs.push(this.personalDataService.user.subscribe(user => {
      if (!user.fireUser) {
        return;
      }
      this.myKey = user.fireUser.uid;
      this.messages$ = this.msgService.getMyMessages(this.myKey, this.xMult);
    }));
  }

  getMore() {
    this.xMult += 1;
    this.messages$ = this.msgService.getMyMessages(this.myKey, this.xMult);
  }

  ngOnInit() {
    this.backgroundContainer = document.getElementById('mat-sidenav-content');
    this.backgroundContainer.classList.add('div__background-image--picture');
  }

  ngOnDestroy(): void {
    this.backgroundContainer.classList.remove('div__background-image--picture');
    this.subs.forEach(x => x.unsubscribe());
  }
}
