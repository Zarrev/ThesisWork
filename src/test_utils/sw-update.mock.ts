import {Subject} from 'rxjs';
import { TestBed, async } from '@angular/core/testing';

export class SwUpdateMock {
  $$availableSubj = new Subject<{available: {hash: string}}>();
  $$activatedSubj = new Subject<{current: {hash: string}}>();

  available = this.$$availableSubj.asObservable();
  activated = this.$$activatedSubj.asObservable();

  activateUpdate = jasmine.createSpy('SwUpdateMock.activateUpdate')
    .and.callFake(() => Promise.resolve());

  checkForUpdate = jasmine.createSpy('SwUpdateMock.checkForUpdate')
    .and.callFake(() => Promise.resolve());

  constructor(public isEnabled: boolean) {}
}
