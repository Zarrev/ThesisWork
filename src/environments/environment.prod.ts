export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyD3_WhUm8SZCf7724re6qrKi0K5kFT47W8',
    authDomain: 'world-of-your-meals.firebaseapp.com',
    databaseURL: 'https://world-of-your-meals.firebaseio.com',
    projectId: 'world-of-your-meals',
    storageBucket: '',
    messagingSenderId: '857105977182',
    appId: '1:857105977182:web:dd690626d165f421'
  },
  openStreetMaps: {
    apiKey: 'key=c9792822df554e659158cb312cfaa5fc',
    nominatimLocationURL: 'https://nominatim.openstreetmap.org/reverse?',
    nominatimSearchURL: 'https://nominatim.openstreetmap.org/search?'
  },
  developer: {
    name: 'Zsolt Barkó',
    email: 'barko.zsolte@gmail.com',
    secondaryEmail: 'barko.zsolt@hallgato.ppke.hu'
  }
};
