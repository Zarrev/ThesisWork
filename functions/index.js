const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');

const config = JSON.parse(process.env.FIREBASE_CONFIG);
config.databaseAuthVariableOverride = { uid: "messaging-service" };
admin.initializeApp(config);

exports.fcmSend = functions.database.ref('/{userId}/messages/{messageId}').onCreate((change, context) => {
  const userId = context.params.userId;
  const payload = {
    notification: {
      title: 'WOYM: Message Notification',
      body: 'You got a Meal from one of our users!',
      icon: '/assets/icons/icon-72x72.png'
    }
  };

  admin.database().ref(`/users/${userId}/fcmToken`).once('value')
    .then(token =>  token.val()).then(userFcmToken => admin.messaging().sendToDevice(userFcmToken, payload))
    .then(res => { console.log('Sent successfully', res); return true; }).catch(err => { console.log(err); return false; })
});
